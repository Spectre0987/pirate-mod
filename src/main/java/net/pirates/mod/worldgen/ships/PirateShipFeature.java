package net.pirates.mod.worldgen.ships;

import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BarrelBlock;
import net.minecraft.block.Blocks;
import net.minecraft.entity.SpawnReason;
import net.minecraft.tileentity.BarrelTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.storage.loot.LootTables;
import net.pirates.mod.blocks.PBlocks;
import net.pirates.mod.config.PiratesConfig;
import net.pirates.mod.entities.CursedPirateEntity;
import net.pirates.mod.entities.PEntities;
import net.pirates.mod.entities.pirate.misc.PirateRank;
import net.pirates.mod.helper.Helper;

public class PirateShipFeature extends Feature<NoFeatureConfig> {

	private static final ResourceLocation STRUCTURE = Helper.createRL("pirates/ships/generic_ship_black");
	
	public PirateShipFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, NoFeatureConfig config) {
		if(worldIn instanceof WorldGenRegion && rand.nextInt(PiratesConfig.SHIP_CHANCE.get()) == 1) {
			WorldGenRegion region = (WorldGenRegion)worldIn;
			
			Template temp = region.getWorld().getStructureTemplateManager().getTemplate(STRUCTURE);
			PlacementSettings settings = new PlacementSettings();
			pos = pos.down(3);
			temp.addBlocksToWorld(region, pos, settings);
			for(BlockInfo info : temp.func_215381_a(pos, settings, Blocks.STRUCTURE_BLOCK)){
				String id = info.nbt.getString("metadata");
				if(id.contentEquals("barrel_east")) {
					region.getWorld().getServer().enqueue(new TickDelayedTask(20, () -> {
						region.getWorld().setBlockState(info.pos, Blocks.BARREL.getDefaultState().with(BarrelBlock.PROPERTY_FACING, Direction.EAST), 3);
						TileEntity te = region.getWorld().getTileEntity(info.pos);
						if(te instanceof BarrelTileEntity) {
							((BarrelTileEntity)te).setLootTable(LootTables.CHESTS_SHIPWRECK_TREASURE, rand.nextLong());
						}
					}));
				}
				else if(id.contentEquals("chain")) {
					worldIn.setBlockState(info.pos, PBlocks.CHAIN.getDefaultState(), 3);
					BlockPos current = new MutableBlockPos(info.pos.down());
					for(int y = 0; y < 255; ++y) {
						if(worldIn.getBlockState(current).getMaterial().isReplaceable()) {
							worldIn.setBlockState(current, PBlocks.CHAIN.getDefaultState(), 3);
							current = current.down();
						}
						else break;
					}
				}
				else if(id.contentEquals("crew")) {
					int amt = 3 + rand.nextInt(5);
					for(int i = 0; i < amt; ++i) {
						CursedPirateEntity ent = PEntities.CURSED_PIRATE.create(region.getWorld());
						ent.setPosition(info.pos.getX() + 0.5, info.pos.getY() + 1, info.pos.getZ() + 0.5);
						ent.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.NATURAL, null, null);
						region.addEntity(ent);
					}
					//captain
					CursedPirateEntity ent = PEntities.CURSED_PIRATE.create(region.getWorld());
					ent.setPosition(info.pos.getX() + 0.5, info.pos.getY() + 1, info.pos.getZ() + 0.5);
					ent.setRank(PirateRank.CAPTAIN);
					ent.onInitialSpawn(worldIn, worldIn.getDifficultyForLocation(pos), SpawnReason.NATURAL, null, null);
					region.addEntity(ent);
					worldIn.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
				}
			}
		}
		return false;
	}

}
