package net.pirates.mod.worldgen.placements;

import java.util.Random;
import java.util.function.Function;
import java.util.stream.Stream;

import com.mojang.datafixers.Dynamic;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.placement.NoPlacementConfig;
import net.minecraft.world.gen.placement.Placement;

public class ShipPlacement extends Placement<NoPlacementConfig> {

   public ShipPlacement(Function<Dynamic<?>, ? extends NoPlacementConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public Stream<BlockPos> getPositions(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generatorIn, Random random, NoPlacementConfig configIn, BlockPos pos) {
		int x = random.nextInt(16);
		int z = random.nextInt(16);
		
		int y = worldIn.getHeight(Heightmap.Type.WORLD_SURFACE_WG, pos.getX() + x, pos.getZ() + z);
		return Stream.of(new BlockPos(x + pos.getX(), y, z + pos.getZ()));
	}

}
