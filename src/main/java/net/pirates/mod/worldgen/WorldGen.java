package net.pirates.mod.worldgen;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.placement.IPlacementConfig;
import net.minecraft.world.gen.placement.NoPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.pirates.mod.Pirates;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.worldgen.placements.ShipPlacement;
import net.pirates.mod.worldgen.ships.PirateShipFeature;

@EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class WorldGen {
	
	public static final Feature<NoFeatureConfig> PIRATE_SHIP = new PirateShipFeature(NoFeatureConfig::deserialize);
	
	public static final Placement<NoPlacementConfig> SHIP_PLACEMENT = new ShipPlacement(NoPlacementConfig::deserialize);
	
	@SubscribeEvent
	public static void registerFeatures(Register<Feature<?>> event) {
		event.getRegistry().registerAll(
				PIRATE_SHIP.setRegistryName(Helper.createRL("generic_pirate_01"))
		);
	}
	
	@SubscribeEvent
	public static void applyFeatures(FMLCommonSetupEvent event) {
		for(Biome b : ForgeRegistries.BIOMES) {
			if(b.getCategory() == Category.OCEAN)
				b.addFeature(Decoration.SURFACE_STRUCTURES, Biome.createDecoratedFeature(PIRATE_SHIP, NoFeatureConfig.NO_FEATURE_CONFIG, SHIP_PLACEMENT, IPlacementConfig.NO_PLACEMENT_CONFIG));
		}
	}

}
