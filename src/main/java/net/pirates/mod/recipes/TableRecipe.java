package net.pirates.mod.recipes;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.pirates.mod.Pirates;

public class TableRecipe {

	ResourceLocation key;
	List<Ingredient> ingredients = Lists.newArrayList();
	private ItemStack result;
	
	private TableRecipe() {}
	
	public boolean matches(ItemStack... key) {
		List<ItemStack> stacks = Lists.newArrayList(key);
		stacks.removeIf(stack -> stack.isEmpty());
		
		if(stacks.size() != this.ingredients.size() || stacks.isEmpty())
			return false;
		
		List<Ingredient> ingredients = Lists.newArrayList(this.ingredients);
		
		for(Ingredient ing : ingredients) {
			
			boolean match = false;
			
			Iterator<ItemStack> stackIt = stacks.iterator();
			while(stackIt.hasNext()) {
				ItemStack stack = stackIt.next();
				if(ing.test(stack)) {
					match = true;
					stackIt.remove();
					break;
				}
			}
			
			if(!match)
				return false;
			
		}
		return true;
		
	}
	
	public ItemStack getResult(){
		return this.result.copy();
	}
	
	public void finailize() {
		this.ingredients = Collections.unmodifiableList(this.ingredients);
	}
	
	public ResourceLocation getKey() {
		return this.key;
	}
	
	public List<Ingredient> getIngredients(){
		return Collections.unmodifiableList(this.ingredients);
	}
	
	public void encode(PacketBuffer buf) {
		//Key
		buf.writeResourceLocation(key);
		//Ingredients
		buf.writeInt(this.ingredients.size());
		for(Ingredient ing : this.ingredients) {
			ing.write(buf);
		}
		//Result
		buf.writeItemStack(result);
	}
	
	public static TableRecipe decode(PacketBuffer buf) {
		TableRecipe recipe = new TableRecipe();
		//Key
		recipe.key = buf.readResourceLocation();
		
		//Ingredients
		int ingSize = buf.readInt();
		recipe.ingredients = Lists.newArrayList();
		for(int i = 0; i < ingSize; ++i) {
			recipe.ingredients.add(Ingredient.read(buf));
		}
		
		//Result
		recipe.result = buf.readItemStack();
		return recipe;
		
	}
	
	public static TableRecipe deserialize(ResourceLocation key, JsonObject recipeData) {
		try {
			
			TableRecipe recipe = new TableRecipe();
			recipe.key = key;
			
			List<Ingredient> ingred = Lists.newArrayList();
			
			for(JsonElement ele : recipeData.get("ingredients").getAsJsonArray()){
				ingred.add(Ingredient.deserialize(ele));
			}
			
			recipe.ingredients.addAll(ingred);
			
			JsonObject results = recipeData.get("result").getAsJsonObject();
			ResourceLocation resultItem = new ResourceLocation(results.get("item").getAsString());
			int count = results.has("count") ? results.get("count").getAsInt() : 1;
			recipe.result = new ItemStack(ForgeRegistries.ITEMS.getValue(resultItem), count);
			
			recipe.finailize();
			return recipe;
			
		}
		catch(Exception e) {
			Pirates.LOGGER.log(Level.ERROR, String.format("Issue parsing recipe %s!", key));
			Pirates.LOGGER.catching(Level.ERROR, e);
		}
		return null;
	}
}
