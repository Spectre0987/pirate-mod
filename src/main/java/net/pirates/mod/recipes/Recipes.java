package net.pirates.mod.recipes;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.pirates.mod.Pirates;

@Mod.EventBusSubscriber(modid = Pirates.MODID)
public class Recipes {

	public static List<TableRecipe> RECIPES = new ArrayList<TableRecipe>();
	public static Comparator<TableRecipe> SORTER = (rec, rec1) -> String.CASE_INSENSITIVE_ORDER.compare(rec.getKey().toString(), rec1.getKey().toString());
	
	@SubscribeEvent
	public static void onServerStart(FMLServerStartingEvent event) {
		
		RECIPES.clear();
		
		try {
			Collection<ResourceLocation> locations = event.getServer().getResourceManager().getAllResourceLocations("pirates_crafting/", s -> s.endsWith(".json"));
			for(ResourceLocation loc : locations) {
				Pirates.LOGGER.log(Level.INFO, "Reading recipe: " + loc);
				InputStream is = event.getServer().getResourceManager().getResource(loc).getInputStream();
				JsonObject doc = new JsonParser().parse(new InputStreamReader(is)).getAsJsonObject();
				RECIPES.add(TableRecipe.deserialize(loc, doc));
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		RECIPES.removeIf(rec -> rec == null);
		RECIPES.sort(SORTER);
		
	}

	public static TableRecipe getByResourceLocation(ResourceLocation key) {
		for(TableRecipe recipe : RECIPES) {
			if(recipe.getKey().equals(key))
				return recipe;
		}
		return null;
	}
}
