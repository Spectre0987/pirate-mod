package net.pirates.mod.tileentites;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.BoatEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;

public class SlingTile extends TileEntity implements ITickableTileEntity{

	private BoatEntity liftingBoat;
	private CompoundNBT capturedBoat;
	
	public SlingTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}

	@Override
	public void tick() {
		if(this.liftingBoat == null && this.capturedBoat == null)
			this.searchForBoat();
		else if(this.liftingBoat != null && this.capturedBoat == null){
			this.liftingBoat.setMotion(new Vec3d(0, 0.1, 0));
			this.captureBoat();
		}
		
	}
	
	public void captureBoat() {
		if(this.capturedBoat == null) {
			for(BoatEntity boat : world.getEntitiesWithinAABB(BoatEntity.class, new AxisAlignedBB(this.getPos()).grow(0.5))) {
				this.capturedBoat = boat.serializeNBT();
				boat.remove();
				return;
			}
		}
	}
	
	public void searchForBoat() {
		for(BoatEntity boat : world.getEntitiesWithinAABB(BoatEntity.class, new AxisAlignedBB(this.getPos()).expand(0, -16, 0))) {
			this.liftingBoat = boat;
			return;
		}
	}
	
	public CompoundNBT getCapturedBoat() {
		return this.capturedBoat;
	}
	
	public Entity createBoat(World world) {
		if(this.capturedBoat == null || this.capturedBoat.isEmpty())
			return null;
		
		EntityType<?> type = ForgeRegistries.ENTITIES.getValue(new ResourceLocation(this.capturedBoat.getString("entity_id")));
		if(type == null)
			return null;
		
		Entity e = type.create(world);
		e.deserializeNBT(this.capturedBoat);
		return e;
	}
	
	public CompoundNBT createBoatTag(BoatEntity ent) {
		CompoundNBT tag = ent.serializeNBT();
		tag.putString("entity_id", ent.getType().getRegistryName().toString());
		return tag;
	}

}
