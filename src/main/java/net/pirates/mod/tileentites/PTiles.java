package net.pirates.mod.tileentites;

import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.PBlocks;
import net.pirates.mod.blocks.TileBlock;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PTiles {
	
	public static final DeferredRegister<TileEntityType<?>> TILES = new DeferredRegister<>(ForgeRegistries.TILE_ENTITIES, Pirates.MODID);
	
	public static final RegistryObject<TileEntityType<CrateTile>> CRATE = TILES.register("crate", () -> register(CrateTile::new, PBlocks.CRATE));
	public static final RegistryObject<TileEntityType<CraftingTableTile>> CRAFTING_TABLE = TILES.register("crafting_bench", () -> register(CraftingTableTile::new, PBlocks.CRAFTING_TABLE));
	public static final RegistryObject<TileEntityType<ShipWheelTile>> SHIP_WHEEL = TILES.register("ship_wheel", () -> register(ShipWheelTile::new, PBlocks.SHIPS_WHEEL));
	public static final RegistryObject<TileEntityType<SlingTile>> SLING = TILES.register("boat_sling", () -> register(() -> new SlingTile(PTiles.SLING.get()), PBlocks.SLING));
	
	public static <T extends TileEntity> TileEntityType<T> register(Supplier<T> tile, Block... blocks){
		TileEntityType.Builder<T> builder = TileEntityType.Builder.create(tile, blocks);
		TileEntityType<T> type = builder.build(null);
		for(Block block : blocks) {
			if(block instanceof TileBlock) {
				((TileBlock)block).setTileType(type);
			}
		}
		return type;
	} 

}
