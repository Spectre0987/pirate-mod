package net.pirates.mod.tileentites;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class ShipWheelTile extends TileEntity{

	public ShipWheelTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ShipWheelTile() {
		super(PTiles.SHIP_WHEEL.get());
	}

}
