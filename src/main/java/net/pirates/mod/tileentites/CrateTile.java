package net.pirates.mod.tileentites;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.pirates.mod.misc.CrateData;

public class CrateTile extends TileEntity{
	
	private CrateData data;
	
	public CrateTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public CrateTile() {
		super(PTiles.CRATE.get());
	}
	
	public void setCrateData(CrateData data) {
		this.data = data;
		this.markDirty();
	}
	
	public CrateData getCrateData() {
		return this.data;
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		
		if(compound.contains("crate"))
			this.data = new CrateData(compound.getCompound("crate"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		
		if(data != null)
			compound.put("crate", this.data.serializeNBT());
		
		return super.write(compound);
	}

}
