package net.pirates.mod.tileentites;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class CraftingTableTile extends TileEntity{

	private ItemStackHandler handler = new ItemStackHandler(5);
	private LazyOptional<ItemStackHandler> supplier = LazyOptional.of(() -> handler);
	
	public CraftingTableTile(TileEntityType<?> type) {
		super(type);
	}
	
	public CraftingTableTile() {
		this(PTiles.CRAFTING_TABLE.get());
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if(compound.contains("inv"))
			handler.deserializeNBT(compound.getCompound("inv"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.put("inv", this.handler.serializeNBT());
		return super.write(compound);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? supplier.cast() : super.getCapability(cap, side);
	}
}
