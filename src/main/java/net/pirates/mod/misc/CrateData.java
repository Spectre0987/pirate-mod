package net.pirates.mod.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.pirates.mod.Pirates;

public class CrateData implements INBTSerializable<CompoundNBT>{
	
	public TranslationTextComponent trans;
	public int basePrice = 0;
	public boolean isFood = false;
	
	public CrateData(String key, int price, boolean food) {
		this.trans = new TranslationTextComponent("crates." + Pirates.MODID + "." + key);
		this.basePrice = price;
		this.isFood = food;
	}
	
	public CrateData(CompoundNBT data) {
		this.deserializeNBT(data);
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("trans", this.trans.getKey());
		tag.putInt("price", this.basePrice);
		tag.putBoolean("food", this.isFood);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		if(tag == null || tag.isEmpty())
			return;
		this.trans = new TranslationTextComponent(tag.getString("trans"));
		this.basePrice = tag.getInt("price");
		this.isFood = tag.getBoolean("food");
	}

}
