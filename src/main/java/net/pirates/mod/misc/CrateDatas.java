package net.pirates.mod.misc;

public class CrateDatas {

	public static final CrateData SPICES = new CrateData("spices", 100, false);
	public static final CrateData COFFEE = new CrateData("coffee", 50, false);
	public static final CrateData SILK = new CrateData("silk", 200, false);
	public static final CrateData TOBACCO = new CrateData("tobacco", 125, false);
	public static final CrateData PRECIOUS_METALS = new CrateData("precious_metals", 500, false);
	public static final CrateData ARMS = new CrateData("arms", 75, false);
}
