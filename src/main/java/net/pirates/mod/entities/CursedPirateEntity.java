package net.pirates.mod.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

public class CursedPirateEntity extends AbstractPirateEntity{

	private static final DataParameter<Boolean> SKELETAL = EntityDataManager.createKey(CursedPirateEntity.class, DataSerializers.BOOLEAN);

	public CursedPirateEntity(EntityType<? extends MonsterEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	@Override
	public void tick() {
		super.tick();
		if(!world.isRemote) {
			if(this.world.isDaytime() && this.isSkeletal())
				this.setSkeletal(false);
			else if(!world.isDaytime() && !this.isSkeletal())
				this.setSkeletal(true);
		}
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 0.3, false));
		this.goalSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, true));
	}

	@Override
	protected void registerAttributes() {
		super.registerAttributes();
		//this.getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(4.0);
	}

	@Override
	protected void registerData() {
		super.registerData();
		this.getDataManager().register(SKELETAL, false);
	}
	
	public boolean isSkeletal() {
		return this.getDataManager().get(SKELETAL);
	}
	
	public void setSkeletal(boolean skeletal) {
		this.getDataManager().set(SKELETAL, skeletal);
	}

}
