package net.pirates.mod.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class GrappleHookEntity extends ThrowableEntity{

	private boolean pulling = false;
	
	public GrappleHookEntity(EntityType<? extends ThrowableEntity> type, World world) {
		super(type, world);
	}
	
	public GrappleHookEntity(World world) {
		this(PEntities.GRAPPLE, world);
	}
	
	public GrappleHookEntity(EntityType<? extends ThrowableEntity> type, LivingEntity ent, World worldIn) {
		super(type, ent, worldIn);
		this.shoot(ent, ent.rotationPitch, ent.prevRotationYawHead, 0, 2, 0);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		this.setMotion(Vec3d.ZERO);
		this.pulling = true;
	}

	@Override
	public void tick() {
		super.tick();
		if(this.pulling) {
			this.setMotion(Vec3d.ZERO);
			if(this.getThrower() != null) {
				Vec3d mot = this.getPositionVector().subtract(this.getThrower().getPositionVec());
				this.getThrower().setMotion(mot.scale(0.1));
			}
			else this.remove();
		}
		
		if(this.ticksExisted > (20 * 60))
			this.remove();
	}

	@Override
	protected void registerData() {}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

}
