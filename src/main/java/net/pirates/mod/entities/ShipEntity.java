package net.pirates.mod.entities;

import java.util.Map.Entry;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.Pose;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;
import net.pirates.mod.helper.BlockStore;
import net.pirates.mod.helper.ShipHelper;
import net.pirates.mod.net.Network;
import net.pirates.mod.ship.BlockShell;
import net.pirates.mod.ship.IHaveShell;
import net.pirates.mod.ship.ShipSize;

public class ShipEntity extends Entity implements IEntityAdditionalSpawnData, IHaveShell{
	
	private BlockShell shell = new BlockShell(BlockPos.ZERO);
	private Vec3d wheelPos = Vec3d.ZERO;
	private Direction assembleDirection = Direction.NORTH;
	private EntitySize shipSize;
	
	public float rotationRoll;
	public float prevRotationRoll;

	public ShipEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public ShipEntity(World worldIn) {
		super(PEntities.SHIP, worldIn);
	}

	@Override
	protected void registerData() {}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getShell().deserializeNBT(compound.getCompound("shell"));
		this.assembleDirection = Direction.byHorizontalIndex(compound.getInt("assembled"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.put("shell", this.getShell().serializeNBT());
		compound.putInt("assembled", this.assembleDirection.getHorizontalIndex());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	public void setWheelPosition(Vec3d pos) {
		this.wheelPos = this.getPositionVec().subtract(pos);
	}

	@Override
	public void writeSpawnData(PacketBuffer buf) {
		shell.encode(buf);
		buf.writeInt(this.assembleDirection.ordinal());
		Network.writeVec3d(this.wheelPos, buf);
	}

	@Override
	public void readSpawnData(PacketBuffer buf) {
		shell.decode(buf);
		this.assembleDirection = Direction.values()[buf.readInt()];
		this.wheelPos = Network.readVec3d(buf);
	}

	@Override
	public void updatePassenger(Entity passenger) {
		super.updatePassenger(passenger);
		
		double angle = Math.toRadians(this.rotationYaw);
		
		double x = this.posX + (Math.sin(angle) * wheelPos.x);
		double z = this.posZ + (Math.cos(angle) * wheelPos.z);
		
		//passenger.setPosition(x, posY + 5, z);
		
	}

	@Override
	public void setShell(BlockShell shell) {
		this.shell = shell;
	}

	@Override
	public BlockShell getShell() {
		return this.shell;
	}

	@Override
	public void tick() {
		super.tick();
		
		//Handle Drive
		if(!this.getPassengers().isEmpty() && this.getPassengers().get(0) instanceof LivingEntity) {
			this.handleDrive((LivingEntity)this.getPassengers().get(0));
		}
		else this.disassemble();
		
		//If more than 2 blocks deep
		if(world.getBlockState(this.getPosition().up(2)).getFluidState().isSource())
			this.setMotion(this.getMotion().add(0, 0.05, 0));
		else {
			//Gravity
			if(!this.isAirBorne && !this.hasNoGravity())
				this.setMotion(this.getMotion().add(0, -0.05, 0));
		}
		
		this.setMotion(this.getMotion().scale(0.9));
		this.move(MoverType.SELF, this.getMotion());
		
		this.prevRotationRoll = this.rotationRoll;
		this.rotationRoll = (float)Math.sin(this.ticksExisted * 0.05);
		
		if(this.shipSize == null) {
			this.shipSize = new ShipSize(this.shell.blocks.keySet()).getSize();
			this.recalculateSize();
		}
		
	}
	
	private void handleDrive(LivingEntity captain) {
		if(captain.moveForward > 0)
			this.setMotion(this.getMotion().add(this.getLookVec().scale(0.05)));
		
		//Turn
		if(captain.moveStrafing > 0)
			this.rotationYaw -= 1;
		else if(captain.moveStrafing < 0)
			this.rotationYaw += 1;
	}

	public void setFacing(Direction direction) {
		this.assembleDirection = direction;
	}
	
	public Direction getAssembledDirection() {
		return this.assembleDirection;
	}
	
	public void disassemble() {
		if(!world.isRemote) {
			
			Rotation rot = ShipHelper.getRotationFromShip(this.getHorizontalFacing().getOpposite(), this.assembleDirection);
			for(Entry<BlockPos, BlockStore> entry : this.shell.blocks.entrySet()) {
				BlockPos pos = entry.getKey().subtract(this.shell.getOffset());
				pos = ShipHelper.rotatePos(pos, rot);
				pos = this.getPosition().add(pos);
				world.setBlockState(pos, entry.getValue().getState().rotate(rot));
				
				TileEntity te = world.getTileEntity(pos);
				CompoundNBT data = entry.getValue().getTileData();
				if(te != null && data != null) {
					te.deserializeNBT(data);
					te.setWorld(world);
					te.setPos(pos);
				}
			}
			
			this.remove();
		}
	}

	@Override
	public boolean shouldRiderSit() {
		return false;
	}

	@Override
	public boolean canBeRiddenInWater(Entity rider) {
		return true;
	}

	@Override
	public boolean canBeRiddenInWater() {
		return true;
	}

	public void setShipSize(EntitySize size) {
		this.shipSize = size;
		System.out.println("Ship bounds set to " + size);
	}

	@Override
	public EntitySize getSize(Pose poseIn) {
		return this.shipSize != null ? this.shipSize : super.getSize(poseIn);
	}
}
