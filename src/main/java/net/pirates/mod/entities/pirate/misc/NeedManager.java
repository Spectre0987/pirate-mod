package net.pirates.mod.entities.pirate.misc;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.util.DamageSource;
import net.pirates.mod.entities.pirate.misc.needs.HungerState;

public class NeedManager {

	CreatureEntity entity;
	int hungerTicks = 0;
	
	public void tick() {
		
		if(this.hungerTicks > 0) {
			--this.hungerTicks;
			
			//Starve damage
			if(this.hungerTicks < 0 && entity.ticksExisted + 4 % 20 == 0) {
				this.entity.attackEntityFrom(DamageSource.STARVE, 1.0F);
			}
			
		}
		
	}
	
	public HungerState getHungerState() {
		for(HungerState state : HungerState.values()) {
			if(this.hungerTicks > state.getHungerTicks())
				return state;
		}
		return HungerState.STARVING;
	}
}
