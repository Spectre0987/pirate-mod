package net.pirates.mod.entities.pirate.misc;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.AbstractPirateEntity;

public interface IPirateRank {

	void fillEquipmentSlot(EquipmentSlotType slot, AbstractPirateEntity pirate);
	ResourceLocation[] getTextures();
}
