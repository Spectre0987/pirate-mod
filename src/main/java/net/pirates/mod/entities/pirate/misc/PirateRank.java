package net.pirates.mod.entities.pirate.misc;

public enum PirateRank {
	DECKHAND(new DeckhandRank()),
	CAPTAIN(new CaptainRank());
	
	IPirateRank rank;
	
	PirateRank(IPirateRank rank){
		this.rank = rank;
	}
	
	public IPirateRank getRankObject() {
		return this.rank;
	}
}
