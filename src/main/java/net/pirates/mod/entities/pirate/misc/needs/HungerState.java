package net.pirates.mod.entities.pirate.misc.needs;

public enum HungerState {

	//10 mins
	FULL(20 * 60 * 10),
	//5 mins
	FINE(20 * 60 * 5),
	//2 minutes
	HUNGRRY(20 * 60 * 2),
	//1 minute till starvation
	STARVING(20 * 60);
	
	
	int ticks;
	
	HungerState(int ticks){
		this.ticks = ticks;
	}

	public int getHungerTicks() {
		return ticks;
	}
}
