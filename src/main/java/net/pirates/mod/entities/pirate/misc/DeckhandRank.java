package net.pirates.mod.entities.pirate.misc;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.AbstractPirateEntity;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.items.PItems;

public class DeckhandRank implements IPirateRank{

	public static ResourceLocation[] TEXTURES = new ResourceLocation[] {
			Helper.createRL("textures/entity/pirates/pirate_deckhand_001.png"),
			Helper.createRL("textures/entity/pirates/pirate_deckhand_002.png"),
			Helper.createRL("textures/entity/pirates/pirate_deckhand_003.png"),
			Helper.createRL("textures/entity/pirates/pirate_deckhand_004.png")};
	
			
	@Override
	public void fillEquipmentSlot(EquipmentSlotType slot, AbstractPirateEntity pirate) {
		if(slot == EquipmentSlotType.HEAD && pirate.getRNG().nextDouble() < 0.2)
			pirate.setItemStackToSlot(slot, new ItemStack(PItems.TRICORN));
		if(slot == EquipmentSlotType.MAINHAND)
			pirate.setItemStackToSlot(slot, new ItemStack(pirate.getRNG().nextDouble() < 0.2 ? Items.IRON_SWORD : PItems.CUTLASS));
	}

	@Override
	public ResourceLocation[] getTextures() {
		return TEXTURES;
	}

}
