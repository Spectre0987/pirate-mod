package net.pirates.mod.entities.pirate.misc;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.AbstractPirateEntity;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.items.PItems;

public class CaptainRank implements IPirateRank{

	public static final ResourceLocation[] TEXTURES = new ResourceLocation[] {
			Helper.createRL("textures/entity/pirates/pirate_midship_001.png"),
			Helper.createRL("textures/entity/pirates/pirate_midship_002.png"),
			Helper.createRL("textures/entity/pirates/pirate_midship_003.png"),
			Helper.createRL("textures/entity/pirates/pirate_midship_004.png")
	};
	
	@Override
	public void fillEquipmentSlot(EquipmentSlotType slot, AbstractPirateEntity pirate) {
		if(slot == EquipmentSlotType.HEAD)
			pirate.setItemStackToSlot(slot, new ItemStack(PItems.TRICORN));
		if(slot == EquipmentSlotType.MAINHAND)
			pirate.setItemStackToSlot(slot, new ItemStack(PItems.rapier));
	}

	@Override
	public ResourceLocation[] getTextures() {
		return TEXTURES;
	}

}
