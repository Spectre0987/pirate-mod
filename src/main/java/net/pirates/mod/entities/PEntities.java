package net.pirates.mod.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.IFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.pirates.mod.Pirates;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PEntities {
	
	public static EntityType<BulletEntity> BULLET;
	public static EntityType<BombEntity> BOMB;
	public static EntityType<GrappleHookEntity> GRAPPLE;
	public static EntityType<ShipEntity> SHIP;
	public static EntityType<CannonBallEntity> CANNON_BALL;
	
	public static EntityType<CursedPirateEntity> CURSED_PIRATE;
			
	@SubscribeEvent
	public static void register(RegistryEvent.Register<EntityType<?>> event) {
		event.getRegistry().registerAll(
				BULLET = register(BulletEntity::new, "bullet", EntityClassification.MISC, 0.25F, 0.25F, 5, true),
				BOMB = register(BombEntity::new, "bomb", EntityClassification.MISC, 0.25F, 0.25F, 5, true),
				GRAPPLE = register(GrappleHookEntity::new, "grapple_hook", EntityClassification.MISC, 0.25F, 0.25F, 10, true),
				SHIP = register(ShipEntity::new, "ship", EntityClassification.MISC, 8, 8, 1, true),
				CANNON_BALL = register(CannonBallEntity::new, "cannon_ball", EntityClassification.MISC, 0.375F, 0.375F, 1, true),
				
				CURSED_PIRATE = register(CursedPirateEntity::new, "cursed_pirate", EntityClassification.CREATURE, 0.8F, 1.7F)
		);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Entity> EntityType<T> register(IFactory<T> fact, String name, EntityClassification classify, float width, float height, int update, boolean sendVelo) {
		EntityType.Builder<T> builder = EntityType.Builder.create(fact, EntityClassification.MISC);
		builder.setShouldReceiveVelocityUpdates(sendVelo);
		builder.setTrackingRange(64);
		builder.setUpdateInterval(update);
		builder.size(width, height);
		return (EntityType<T>)builder.build("").setRegistryName(new ResourceLocation(Pirates.MODID, name));
	}
	
	public static <T extends Entity> EntityType<T> register(IFactory<T> fact, String name, EntityClassification classify, float width, float height) {
		return PEntities.register(fact, name, classify, width, height, 1, false);
	}

}
