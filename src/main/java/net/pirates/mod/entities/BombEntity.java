package net.pirates.mod.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class BombEntity extends ThrowableEntity{

	public BombEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public BombEntity(World worldIn) {
		this(PEntities.BOMB, worldIn);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		if(!world.isRemote) {
			world.createExplosion(this, posX, posY, posZ, 2F, Mode.BREAK);
			this.remove();
		}
	}

	@Override
	protected void registerData() {}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

}
