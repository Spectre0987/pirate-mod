package net.pirates.mod.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class CannonBallEntity extends ThrowableEntity{

	public CannonBallEntity(EntityType<? extends ThrowableEntity> type, World world) {
		super(type, world);
	}
	
	public CannonBallEntity(World world) {
		this(PEntities.CANNON_BALL, world);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		if(!world.isRemote) {
			world.createExplosion(this, posX, posY, posZ, 3, Mode.BREAK);
			this.remove();
		}
	}

	@Override
	protected void registerData() {}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

}
