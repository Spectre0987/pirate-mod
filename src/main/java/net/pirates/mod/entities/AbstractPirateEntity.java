package net.pirates.mod.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.RandomWalkingGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.pirates.mod.entities.pirate.misc.PirateRank;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.serializers.PDataSerializers;

public abstract class AbstractPirateEntity extends MonsterEntity{

	private static final DataParameter<ResourceLocation> TEXTURE = EntityDataManager.createKey(AbstractPirateEntity.class, PDataSerializers.RESOURCE_LOCATION);
	private PirateRank rank = PirateRank.DECKHAND;
	
	public AbstractPirateEntity(EntityType<? extends MonsterEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		if(compound.contains("rank"))
			this.rank = PirateRank.valueOf(compound.getString("rank"));
		if(compound.contains("texture"))
			this.dataManager.set(TEXTURE, new ResourceLocation(compound.getString("texture")));
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		compound.putString("rank", this.rank.name());
		compound.putString("texture", this.getDataManager().get(TEXTURE).toString());
	}

	@Override
	public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		if(!world.isRemote) {
			this.getDataManager().set(TEXTURE, this.rank.getRankObject().getTextures()[this.rand.nextInt(this.rank.getRankObject().getTextures().length)]);
			
			for(EquipmentSlotType type : EquipmentSlotType.values()) {
				this.getRank().getRankObject().fillEquipmentSlot(type, this);
			}
		}
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	public ResourceLocation getTexture() {
		return this.getDataManager().get(TEXTURE);
	}
	
	public PirateRank getRank() {
		return this.rank;
	}
	
	public void setRank(PirateRank rank) {
		this.rank = rank;
	}
	
	@Override
	protected void registerGoals() {
		super.registerGoals();
		this.goalSelector.addGoal(5, new RandomWalkingGoal(this, 0.4));
	}

	@Override
	protected void registerData() {
		super.registerData();
		this.dataManager.register(TEXTURE, Helper.createRL("textures/entity/pirates/pirate_deckhand_001.png"));
	}
	
	@Override
	public boolean canDespawn(double distanceToClosestPlayer) {
		return false;
	}

}
