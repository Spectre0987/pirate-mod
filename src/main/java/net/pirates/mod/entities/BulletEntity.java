package net.pirates.mod.entities;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class BulletEntity extends ThrowableEntity{
	
	private float damage = 10F;
	
	public BulletEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public BulletEntity(World world) {
		this(PEntities.BULLET, world);
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		if(!world.isRemote) {
			if(result instanceof EntityRayTraceResult) {
				Entity hit = ((EntityRayTraceResult)result).getEntity();
				hit.attackEntityFrom(DamageSource.GENERIC, this.damage);
				this.remove();
			}
			else if(result instanceof BlockRayTraceResult) {
				BlockPos pos = ((BlockRayTraceResult)result).getPos();
				BlockState state = world.getBlockState(pos);
				if(state.isSolid()) {
					remove();
				}
				else if(state.getMaterial() == Material.GLASS) {
					world.setBlockState(pos, Blocks.AIR.getDefaultState());
				}
			}
		}
	}

	@Override
	protected void registerData() {}

	@Override
	public void tick() {
		super.tick();
		if(world.isRemote) {
			world.addParticle(ParticleTypes.SMOKE, posX, posY, posZ, 0, 0, 0);
		}
	}
	
	public void setDamage(float dam) {
		this.damage = dam;
	}
	
	public float getDamage() {
		return this.damage;
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		compound.putFloat("damage", this.damage);
		super.writeAdditional(compound);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		this.damage = compound.getFloat("damage");
	}

}
