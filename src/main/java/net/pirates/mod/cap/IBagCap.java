package net.pirates.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.IItemHandlerModifiable;

public interface IBagCap extends IItemHandlerModifiable, INBTSerializable<CompoundNBT>{

}
