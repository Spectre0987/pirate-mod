package net.pirates.mod.cap;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.items.ItemStackHandler;
import net.pirates.mod.net.Network;
import net.pirates.mod.net.packets.UpdatePlayerMessage;

public class PlayerDataCapability implements IPlayerData{

	PlayerEntity player;
	ItemStackHandler handler = new ItemStackHandler(4);
	
	public PlayerDataCapability(PlayerEntity player) {
		this.player = player;
	}

	public ItemStackHandler getClothing() {
		return this.handler;
	}

	@Override
	public void tick() {
		
	}

	@Override
	public int getBankGold() {
		return 0;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("inv", this.handler.serializeNBT());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.handler.deserializeNBT(tag.getCompound("inv"));
	}
	
	public void update() {
		Network.CHANNEL.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> this.player), new UpdatePlayerMessage(player.getEntityId(), this.serializeNBT()));
	}

	@Override
	public ItemStack getStackForSlot(PirateEquipSlot slot) {
		return this.handler.getStackInSlot(slot.ordinal());
	}

	@Override
	public void setStackForSlot(PirateEquipSlot slot, ItemStack stack) {
		// TODO Auto-generated method stub
		
	}

}
