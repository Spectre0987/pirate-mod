package net.pirates.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

public class Capabilities {

	@CapabilityInject(IGun.class)
	public static Capability<IGun> GUN = null;
	
	@CapabilityInject(IPlayerData.class)
	public static Capability<IPlayerData> PLAYER_DATA = null;
	
	@CapabilityInject(IBagCap.class)
	public static Capability<IBagCap> BAG = null;
	
	
	//Gun Stuff
	public static class GunStorage implements IStorage<IGun>{

		@Override
		public INBT writeNBT(Capability<IGun> capability, IGun instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IGun> capability, IGun instance, Direction side, INBT nbt) {
			if(nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class GunProvider implements ICapabilitySerializable<CompoundNBT>{

		private IGun gun;
		
		public GunProvider(IGun cap) {
			this.gun = cap;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == GUN ? (LazyOptional<T>)LazyOptional.of(() -> this.gun) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return gun.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			gun.deserializeNBT(nbt);
		}
		
	}
	
	//Bag
	
	public static class BagStorage implements IStorage<IBagCap>{

		@Override
		public INBT writeNBT(Capability<IBagCap> capability, IBagCap instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IBagCap> capability, IBagCap instance, Direction side, INBT nbt) {
			if(nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class BagProvider implements ICapabilitySerializable<CompoundNBT>{

		private IBagCap bag;
		
		public BagProvider(IBagCap cap) {
			this.bag = cap;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == GUN ? (LazyOptional<T>)LazyOptional.of(() -> this.bag) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return bag.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			bag.deserializeNBT(nbt);
		}
		
	}
}
