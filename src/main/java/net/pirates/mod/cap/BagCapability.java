package net.pirates.mod.cap;

import java.util.function.Predicate;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.items.ItemStackHandler;

public class BagCapability implements IBagCap{

	private ItemStackHandler handler;
	private Predicate<ItemStack> filter;
	
	public BagCapability(int size, Predicate<ItemStack> filter) {
		this.handler = new ItemStackHandler(size);
		this.filter = filter;
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		handler.setStackInSlot(slot, stack);
	}

	@Override
	public int getSlots() {
		return handler.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return handler.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		return handler.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		return handler.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return handler.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return this.filter.test(stack);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return handler.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.handler.deserializeNBT(nbt);
	}
	
}
