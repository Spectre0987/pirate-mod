package net.pirates.mod.cap;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;

public interface IGun extends INBTSerializable<CompoundNBT>{

	void setCartridges(int cartridge);
	int getCartridges();
	
	void setLoadedState(EnumGunState state);
	EnumGunState getLoadedState();
	
	void shoot(World world, ItemStack stack, LivingEntity entity);
	
	public static enum EnumGunState{
		LOADED,
		NOT_LOADED
	}
	
}
