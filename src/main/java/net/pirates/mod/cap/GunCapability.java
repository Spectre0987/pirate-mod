package net.pirates.mod.cap;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;
import net.pirates.mod.entities.BulletEntity;
import net.pirates.mod.entities.PEntities;
import net.pirates.mod.net.Network;
import net.pirates.mod.net.packets.SyncCapMessage;

public class GunCapability implements IGun{

	private int cartridges = 0;
	private EnumGunState state = EnumGunState.NOT_LOADED;
	
	public GunCapability() {}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		
	}

	@Override
	public void shoot(World world, ItemStack stack, LivingEntity entity) {
		if(!world.isRemote) {
			if(this.shouldFire()) {
				world.playSound(null, entity.getPosition(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.PLAYERS, 0.5F, 1);
				BulletEntity ball = PEntities.BULLET.create(world);
				ball.setPosition(entity.posX, entity.posY + entity.getEyeHeight(), entity.posZ);
				ball.shoot(entity, entity.rotationPitch, entity.rotationYawHead, 0, 4F, 0);
				ball.setDamage(21);
				world.addEntity(ball);
				this.setCartridges(this.getCartridges() - 1);
				if(entity instanceof ServerPlayerEntity) {
					this.sendToClient((ServerPlayerEntity)entity);
					stack.attemptDamageItem(1, entity.getRNG(), (ServerPlayerEntity)entity);
				}
			}
			else {
				world.playSound(null, entity.getPosition(), SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.PLAYERS, 1F, 1F);
			}
		}
	}
	
	private boolean shouldFire() {
		return this.getCartridges() > 0;
	}
	
	private void sendToClient(ServerPlayerEntity player) {
		Network.CHANNEL.send(PacketDistributor.PLAYER.with(() -> player), new SyncCapMessage(this.serializeNBT()));
	}

	@Override
	public void setCartridges(int cartridge) {
		this.cartridges = cartridge;
	}

	@Override
	public int getCartridges() {
		return this.cartridges;
	}

	@Override
	public void setLoadedState(EnumGunState state) {
		this.state = state;
	}

	@Override
	public EnumGunState getLoadedState() {
		return state;
	}

}
