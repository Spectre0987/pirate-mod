package net.pirates.mod.cap;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;

public interface IPlayerData extends INBTSerializable<CompoundNBT>{
	
	public ItemStackHandler getClothing();
	public ItemStack getStackForSlot(PirateEquipSlot slot);
	public void setStackForSlot(PirateEquipSlot slot, ItemStack stack);
	
	public void tick();
	
	public int getBankGold();
	
	public static enum PirateEquipSlot{
		HAT,
		EYE,
		LEG
	}

	public static class Storage implements Capability.IStorage<IPlayerData>{

		@Override
		public INBT writeNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side, INBT nbt) {
			if(nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT)nbt);
		}

	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		IPlayerData data;
		LazyOptional<IPlayerData> dataHolder = LazyOptional.of(() -> data);
		
		public Provider(IPlayerData data) {
			this.data = data;
		}
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.PLAYER_DATA ? this.dataHolder.cast() : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return this.data.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			this.data.deserializeNBT(nbt);
		}
		
	}
}
