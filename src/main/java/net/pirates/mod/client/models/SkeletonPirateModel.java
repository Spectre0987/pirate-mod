package net.pirates.mod.client.models;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.pirates.mod.entities.CursedPirateEntity;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SkeletonPirateModel<T> extends BipedModel<CursedPirateEntity> {

	public SkeletonPirateModel() {
		super();
		textureWidth = 64;
		textureHeight = 32;

		bipedBody = new RendererModel(this);
		this.bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedBody.cubeList.add(new ModelBox(this.bipedBody, 16, 16, -4.0F, 0.0F, -2.0F, 8, 12, 4, 0.0F, true));

		bipedHead = new RendererModel(this);
		this.bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedHead.cubeList.add(new ModelBox(this.bipedHead, 0, 0, -4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F, true));

		bipedRightArm = new RendererModel(this);
		this.bipedRightArm.setRotationPoint(5.0F, 2.0F, 0.0F);
		this.bipedRightArm.cubeList.add(new ModelBox(this.bipedRightArm, 40, 16, -1.0F, -2.0F, -1.0F, 2, 12, 2, 0.0F, true));

		bipedLeftArm = new RendererModel(this);
		this.bipedLeftArm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		this.bipedLeftArm.cubeList.add(new ModelBox(this.bipedLeftArm, 40, 16, -1.0F, -2.0F, -1.0F, 2, 12, 2, 0.0F, false));

		bipedRightLeg = new RendererModel(this);
		this.bipedRightLeg.setRotationPoint(2.0F, 12.0F, 0.0F);
		this.bipedRightLeg.cubeList.add(new ModelBox(this.bipedRightLeg, 0, 16, -1.0F, 0.0F, -1.0F, 2, 12, 2, 0.0F, true));

		bipedLeftLeg = new RendererModel(this);
		this.bipedLeftLeg.setRotationPoint(-2.0F, 12.0F, 0.0F);
		this.bipedLeftLeg.cubeList.add(new ModelBox(this.bipedLeftLeg, 0, 16, -1.0F, 0.0F, -1.0F, 2, 12, 2, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}