package net.pirates.mod.client.models.clothes;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.player.PlayerEntity;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TricornModel extends BipedModel<PlayerEntity> {
	//private final RendererModel bipedHead;

	public TricornModel() {
		textureWidth = 64;
		textureHeight = 64;

		this.bipedHead = new RendererModel(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 0, -4.5F, -9.0F, -4.5F, 9, 2, 9, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 21, 21, -5.5F, -10.0F, 0.5F, 1, 3, 5, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 14, 15, 4.5F, -10.0F, 0.5F, 1, 3, 5, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 11, -4.5F, -10.0F, 4.5F, 9, 3, 1, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 15, -1.5F, -11.0F, -6.5F, 3, 3, 4, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}