package net.pirates.mod.client.models.clothes;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.LivingEntity;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class HookSteveModel<T extends LivingEntity> extends BipedModel<T> {
	
	//private final RendererModel bipedRightArm;
	private final RendererModel hook;
	private final RendererModel hook2;

	public HookSteveModel() {
		textureWidth = 32;
		textureHeight = 32;

		bipedRightArm = new RendererModel(this);
		bipedRightArm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		bipedRightArm.cubeList.add(new ModelBox(bipedRightArm, 0, 0, -3.5F, 5.5F, -2.5F, 5, 5, 5, 0.0F, false));
		bipedRightArm.cubeList.add(new ModelBox(bipedRightArm, 4, 10, -1.5F, 10.5F, -0.5F, 1, 2, 1, 0.0F, false));

		hook = new RendererModel(this);
		hook.setRotationPoint(-1.0F, 12.5F, 0.0F);
		bipedRightArm.addChild(hook);
		setRotationAngle(hook, -0.7854F, 0.0F, 0.0F);
		hook.cubeList.add(new ModelBox(hook, 0, 0, -0.5F, -0.3536F, -0.6464F, 1, 3, 1, 0.0F, false));

		hook2 = new RendererModel(this);
		hook2.setRotationPoint(0.0F, 3.2678F, 0.2071F);
		hook.addChild(hook2);
		setRotationAngle(hook2, 1.6144F, 0.0F, 0.0F);
		hook2.cubeList.add(new ModelBox(hook2, 0, 10, -0.5F, -0.8256F, 0.658F, 1, 3, 1, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}