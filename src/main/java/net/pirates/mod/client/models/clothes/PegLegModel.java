package net.pirates.mod.client.models.clothes;

import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.LivingEntity;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class PegLegModel<T extends LivingEntity> extends PlayerModel<T> {

	public PegLegModel() {
		super(0.0625F, false);
		textureWidth = 32;
		textureHeight = 32;

		bipedLeftLeg = new RendererModel(this);
		bipedLeftLeg.setRotationPoint(1.9F, 12.0F, 0.0F);
		bipedLeftLeg.cubeList.add(new ModelBox(bipedLeftLeg, 0, 0, -2.0F, 0.0F, -2.0F, 4, 5, 4, 0.0F, false));
		bipedLeftLeg.cubeList.add(new ModelBox(bipedLeftLeg, 0, 9, -1.5F, 5.0F, -1.5F, 3, 1, 3, 0.0F, false));
		bipedLeftLeg.cubeList.add(new ModelBox(bipedLeftLeg, 11, 12, -0.5F, 6.0F, -0.5F, 1, 6, 1, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}