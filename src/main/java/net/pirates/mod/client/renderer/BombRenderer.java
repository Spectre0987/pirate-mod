package net.pirates.mod.client.renderer;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.BombEntity;
import net.pirates.mod.items.PItems;

@SuppressWarnings("deprecation")
public class BombRenderer extends EntityRenderer<BombEntity>{

	public static ItemStack RENDER;
	
	public BombRenderer(EntityRendererManager renderManager) {
		super(renderManager);
		RENDER = new ItemStack(PItems.bomb);
	}

	@Override
	public void doRender(BombEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 0.5, z);
		Minecraft.getInstance().getItemRenderer().renderItem(RENDER, TransformType.NONE);
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(BombEntity entity) {
		return null;
	}

}
