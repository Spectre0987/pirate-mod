package net.pirates.mod.client.renderer;

import java.util.Map;
import java.util.Map.Entry;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.pirates.mod.entities.ShipEntity;
import net.pirates.mod.helper.BlockStore;
import net.pirates.mod.helper.Helper;

public class ShipRenderer extends EntityRenderer<ShipEntity>{

	public ShipRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(ShipEntity entity) {
		return null;
	}

	@Override
	public void doRender(ShipEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 0.5, z);
		
		float rot = -entity.rotationYaw - 180 - Helper.getAngleForFacing(entity.getAssembledDirection());
		float prevRot = -entity.prevRotationYaw - 180 - Helper.getAngleForFacing(entity.getAssembledDirection());
		
		GlStateManager.rotated(MathHelper.lerp(partialTicks, prevRot, rot), 0, 1, 0);
		if(entity.getAssembledDirection().getAxis() == Axis.Z)
			GlStateManager.rotated(180, 0, 1, 0);
		
		//Roll
		GlStateManager.rotated(0, 1, 0, 0);
		
		GlStateManager.rotated(entity.rotationRoll, 1, 0, 0);
		
		this.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		GlStateManager.translated(-entity.getShell().getOffset().getX(), -entity.getShell().getOffset().getY(), -entity.getShell().getOffset().getZ());
		
		Map<BlockPos, TileEntity> tileList = Maps.newHashMap();
		
		for(Entry<BlockPos, BlockStore> store : entity.getShell().blocks.entrySet()) {
			Minecraft.getInstance().getBlockRendererDispatcher().renderBlock(store.getValue().getState(), store.getKey(), Minecraft.getInstance().world, bb, entity.world.rand);
			
			TileEntity te = store.getValue().createTile();
			if(te != null)
				tileList.put(store.getKey(), te);
		}
		Tessellator.getInstance().draw();
		
		for(Entry<BlockPos, TileEntity> entry : tileList.entrySet()) {
			GlStateManager.pushMatrix();
			GlStateManager.translated(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
			TileEntityRendererDispatcher.instance.render(entry.getValue(), 0, 0, 0, partialTicks, 0, true);
			GlStateManager.popMatrix();
		}
		
		GlStateManager.popMatrix();
	}

}
