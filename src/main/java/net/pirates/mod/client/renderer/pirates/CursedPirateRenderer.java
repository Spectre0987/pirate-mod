package net.pirates.mod.client.renderer.pirates;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.client.models.SkeletonPirateModel;
import net.pirates.mod.entities.CursedPirateEntity;

public class CursedPirateRenderer extends PirateRenderer<CursedPirateEntity>{

	private static final ResourceLocation SKELETON_TEXTURE = new ResourceLocation("textures/entity/skeleton/skeleton.png");
	private static final SkeletonPirateModel<CursedPirateEntity> SKELETON_MODEL = new SkeletonPirateModel<>();
	private static final PlayerModel<CursedPirateEntity> PLAYER = new PlayerModel<>(0.0625F, false);
	
	public CursedPirateRenderer(EntityRendererManager renderManagerIn) {
		super(renderManagerIn);
	}

	@Override
	protected void renderModel(CursedPirateEntity pirate, float limbSwing, float limbSwingAmount,float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
		
		if(pirate.isSkeletal())
			this.entityModel = SKELETON_MODEL;
		else this.entityModel = PLAYER;
		super.renderModel(pirate, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
	}

	@Override
	protected ResourceLocation getEntityTexture(CursedPirateEntity entity) {
		return entity.isSkeletal() ? SKELETON_TEXTURE : super.getEntityTexture(entity);
	}

}
