package net.pirates.mod.client.renderer.pirates;

import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.ArmorLayer;
import net.minecraft.client.renderer.entity.layers.ArrowLayer;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.AbstractPirateEntity;

public class PirateRenderer<T extends AbstractPirateEntity> extends BipedRenderer<T, BipedModel<T>>{

	public static final PlayerModel<AbstractPirateEntity> STEVE = new PlayerModel<AbstractPirateEntity>(0.0625F, false);
	
	@SuppressWarnings("unchecked")
	public PirateRenderer(EntityRendererManager renderManagerIn) {
		super(renderManagerIn, (PlayerModel<T>) STEVE, 0.5F);
		this.addLayer(new ArrowLayer<>(this));
		this.addLayer(new BipedArmorLayer<>(this, new BipedModel<>(0.5F), new BipedModel<>(1.0F)));
	}

	@Override
	protected ResourceLocation getEntityTexture(T entity) {
		return entity.getTexture();
	}

}
