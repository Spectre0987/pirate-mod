package net.pirates.mod.client.renderer.layers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.client.models.clothes.HookSteveModel;
import net.pirates.mod.helper.Helper;

public class HookLayer<T extends LivingEntity, M extends BipedModel<T>> extends LayerRenderer<T, M>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/entity/clothes/hook.png");
	public final HookSteveModel<T> steveModel = new HookSteveModel<T>();
	
	public HookLayer(LivingRenderer<T, M> renderer) {
		super(renderer);
		this.steveModel.setVisible(false);
		this.steveModel.bipedRightArm.showModel = true;
	}

	@Override
	public void render(T entity, float p_212842_2_, float p_212842_3_, float p_212842_4_, float p_212842_5_, float p_212842_6_, float p_212842_7_, float p_212842_8_) {
		
		/*
		this.getEntityModel().setModelAttributes(steveModel);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		steveModel.render(entity, p_212842_2_, p_212842_3_, p_212842_5_, p_212842_6_, p_212842_7_, p_212842_8_);
		*/
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

}
