package net.pirates.mod.client.renderer.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.pirates.mod.tileentites.SlingTile;

public class BoatSlingRenderer extends TileEntityRenderer<SlingTile> {

	@Override
	public void render(SlingTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		super.render(tile, x, y, z, partialTicks, destroyStage);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.5, z + 0.5);
		
		
		GlStateManager.popMatrix();
		
	}

}
