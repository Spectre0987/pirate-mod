package net.pirates.mod.client.renderer;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.CannonBallEntity;
import net.pirates.mod.items.PItems;

public class CannonBallRenderer extends EntityRenderer<CannonBallEntity>{

	public static final ItemStack STACK = new ItemStack(PItems.CANNON_BALL);
	
	public CannonBallRenderer(EntityRendererManager manager) {
		super(manager);
	}

	@Override
	protected ResourceLocation getEntityTexture(CannonBallEntity entity) {
		return null;
	}

	@Override
	public void doRender(CannonBallEntity p_76986_1_, double x, double y, double z, float p_76986_8_, float p_76986_9_) {
		super.doRender(p_76986_1_, x, y, z, p_76986_8_, p_76986_9_);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		Minecraft.getInstance().getItemRenderer().renderItem(STACK, TransformType.NONE);
		GlStateManager.popMatrix();
		
	}

}
