package net.pirates.mod.client.renderer;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.entities.BulletEntity;

public class BulletRenderer extends EntityRenderer<BulletEntity>{

	public BulletRenderer(EntityRendererManager renderManager) {
		super(renderManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doRender(BulletEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {}

	@Override
	protected ResourceLocation getEntityTexture(BulletEntity entity) {
		return null;
	}

}
