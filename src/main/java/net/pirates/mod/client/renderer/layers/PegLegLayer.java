package net.pirates.mod.client.renderer.layers;

import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.cap.IPlayerData.PirateEquipSlot;
import net.pirates.mod.client.models.clothes.PegLegModel;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.items.PItems;

public class PegLegLayer<T extends LivingEntity, M extends BipedModel<T>> extends LayerRenderer<T, M>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/entity/clothes/peg_leg.png");
	private final PegLegModel<T> model = new PegLegModel<T>();
	
	public PegLegLayer(LivingRenderer<T, M> renderer) {
		super(renderer);
		model.bipedBody.isHidden = 
			model.bipedHeadwear.isHidden = 
			model.bipedHead.isHidden = 
			model.bipedLeftArm.isHidden = 
			model.bipedRightArm.isHidden = 
			model.bipedRightLeg.isHidden = true;
	}


	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	@Override
	public void render(T entity, float p_212842_2_, float p_212842_3_, float p_212842_4_, float p_212842_5_, float p_212842_6_, float p_212842_7_, float p_212842_8_) {
		
		entity.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
			if(cap.getStackForSlot(PirateEquipSlot.LEG).getItem() == PItems.PEG_LEG) {
				this.bindTexture(TEXTURE);
				this.getEntityModel().setModelAttributes(model);
				this.getEntityModel().bipedLeftLeg.isHidden = true;
				model.bipedLeftLeg.isHidden = false;
				model.render(entity, p_212842_2_, p_212842_3_, p_212842_5_, p_212842_6_, p_212842_7_, p_212842_8_);
			}
			else this.getEntityModel().bipedLeftLeg.isHidden = false;
		});
	}


}
