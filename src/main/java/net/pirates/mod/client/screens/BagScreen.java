package net.pirates.mod.client.screens;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.pirates.mod.containers.BagContainer;

public class BagScreen extends ContainerScreen<BagContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation("textures/gui/container/dispenser.png");
	
	public BagScreen(BagContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int w = 176, h = 166;
		this.blit(width / 2 - w / 2, height / 2 - h / 2, 0, 0, w, h);
	}

}
