package net.pirates.mod.client.events.items;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.pirates.mod.Pirates;
import net.pirates.mod.items.PItems;

@Mod.EventBusSubscriber(modid = Pirates.MODID, value = Dist.CLIENT)
public class ItemEvents {

	@SubscribeEvent
	public static void zoom(FOVUpdateEvent event) {
		if(Minecraft.getInstance().player != null ) {
			if(Minecraft.getInstance().player.getActiveItemStack().getItem() == PItems.telescope && Minecraft.getInstance().gameSettings.thirdPersonView == 0) {
				event.setNewfov(event.getFov() / 16.0F);
			}
		}
	}
	
	@SubscribeEvent
	public static void overlay(RenderGameOverlayEvent event) {
		PlayerEntity player = Minecraft.getInstance().player;
		if(player.getActiveItemStack().getItem() == PItems.telescope && Minecraft.getInstance().gameSettings.thirdPersonView == 0) {
			Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation(Pirates.MODID, "textures/overlays/telescope.png"));
			GlStateManager.pushMatrix();
			GlStateManager.enableAlphaTest();
			GlStateManager.enableBlend();
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
			
			double drawSize = Minecraft.getInstance().mainWindow.getScaledHeight();
			double start = Minecraft.getInstance().mainWindow.getScaledWidth() / 2 - drawSize / 2;
			
			bb.pos(start, 0, 0).tex(0, 0).endVertex();
			bb.pos(start, drawSize, 0).tex(0, 1).endVertex();
			bb.pos(start + drawSize, drawSize, 0).tex(1, 1).endVertex();
			bb.pos(start + drawSize, 0, 0).tex(1, 0).endVertex();
			
			bb.pos(0, 0, 0).tex(0, 0).endVertex();
			bb.pos(0, drawSize, 0).tex(0, 0).endVertex();
			bb.pos(start, drawSize, 0).tex(0, 0).endVertex();
			bb.pos(start, 0, 0).tex(0, 0).endVertex();
			
			double end = start + drawSize;
			
			bb.pos(end, 0, 0).tex(0, 0).endVertex();
			bb.pos(end, drawSize, 0).tex(0, 0).endVertex();
			bb.pos(Minecraft.getInstance().mainWindow.getScaledWidth(), drawSize, 0).tex(0, 0).endVertex();
			bb.pos(Minecraft.getInstance().mainWindow.getScaledWidth(), 0, 0).tex(0, 0).endVertex();
			
			Tessellator.getInstance().draw();
			GlStateManager.disableAlphaTest();
			GlStateManager.disableBlend();
			GlStateManager.popMatrix();
			
			event.setCanceled(true);
		}
	}
}
