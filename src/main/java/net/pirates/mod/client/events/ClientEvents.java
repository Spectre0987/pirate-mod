package net.pirates.mod.client.events;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.pirates.mod.Pirates;
import net.pirates.mod.client.guis.container.CraftingScreen;
import net.pirates.mod.client.guis.container.VanityScreen;
import net.pirates.mod.client.renderer.BombRenderer;
import net.pirates.mod.client.renderer.BulletRenderer;
import net.pirates.mod.client.renderer.CannonBallRenderer;
import net.pirates.mod.client.renderer.ShipRenderer;
import net.pirates.mod.client.renderer.layers.HookLayer;
import net.pirates.mod.client.renderer.layers.PegLegLayer;
import net.pirates.mod.client.renderer.pirates.CursedPirateRenderer;
import net.pirates.mod.client.renderer.tiles.BoatSlingRenderer;
import net.pirates.mod.client.screens.BagScreen;
import net.pirates.mod.containers.PContainers;
import net.pirates.mod.entities.BombEntity;
import net.pirates.mod.entities.BulletEntity;
import net.pirates.mod.entities.CannonBallEntity;
import net.pirates.mod.entities.CursedPirateEntity;
import net.pirates.mod.entities.ShipEntity;
import net.pirates.mod.tileentites.SlingTile;

@Mod.EventBusSubscriber(modid = Pirates.MODID, value = Dist.CLIENT, bus = Bus.MOD)
public class ClientEvents {
	
	@SubscribeEvent
	public static void register(FMLClientSetupEvent event) {
		RenderingRegistry.registerEntityRenderingHandler(BombEntity.class, BombRenderer::new);
		RenderingRegistry.registerEntityRenderingHandler(BulletEntity.class, BulletRenderer::new);
		RenderingRegistry.registerEntityRenderingHandler(ShipEntity.class, ShipRenderer::new);
		RenderingRegistry.registerEntityRenderingHandler(CannonBallEntity.class, CannonBallRenderer::new);
		
		RenderingRegistry.registerEntityRenderingHandler(CursedPirateEntity.class, CursedPirateRenderer::new);
		
		ScreenManager.registerFactory(PContainers.BAG, BagScreen::new);
		ScreenManager.registerFactory(PContainers.CRAFTING, CraftingScreen::new);
		ScreenManager.registerFactory(PContainers.VANITY, VanityScreen::new);
		
		for(PlayerRenderer player : Minecraft.getInstance().getRenderManager().getSkinMap().values()) {
			player.addLayer(new PegLegLayer<>(player));
			player.addLayer(new HookLayer<>(player));
		}
		
		ClientRegistry.bindTileEntitySpecialRenderer(SlingTile.class, new BoatSlingRenderer());
		
	}

}
