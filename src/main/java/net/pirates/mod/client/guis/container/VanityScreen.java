package net.pirates.mod.client.guis.container;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.text.ITextComponent;
import net.pirates.mod.containers.VanityContainer;
import net.pirates.mod.helper.Helper;

public class VanityScreen extends ContainerScreen<VanityContainer>{

	public VanityScreen(VanityContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().textureManager.bindTexture(Helper.createRL("textures/screens/containers/vanity.png"));
		
		int gWidth = 176, gHeight = 166;
		
		this.blit(width / 2 - gWidth / 2, height / 2 - gHeight / 2, 0, 0, gWidth, gHeight);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
		
		this.renderPlayer(mouseX, mouseY, partialTicks);
		
	}
	
	public void renderPlayer(int mouseX, int mouseY, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(width / 2 + 15, height / 2 - 20, 20);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(Minecraft.getInstance().player.rotationYaw, 0, 1, 0);
		GlStateManager.scaled(20, 20, 20);
		
			Minecraft.getInstance().getRenderManager().getRenderer(Minecraft.getInstance().player)
				.doRender(Minecraft.getInstance().player, 0, 0, 0, 0, partialTicks);
			
		GlStateManager.popMatrix();
	}

}
