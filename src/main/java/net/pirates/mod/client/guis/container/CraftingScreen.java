package net.pirates.mod.client.guis.container;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.pirates.mod.Pirates;
import net.pirates.mod.client.guis.widgets.ItemButton;
import net.pirates.mod.containers.CraftingContainer;
import net.pirates.mod.containers.slots.ListenerSlot;
import net.pirates.mod.net.CraftingPacket;
import net.pirates.mod.net.Network;
import net.pirates.mod.recipes.Recipes;
import net.pirates.mod.recipes.TableRecipe;

public class CraftingScreen extends ContainerScreen<CraftingContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Pirates.MODID, "textures/screens/containers/crafting.png");
	
	public CraftingScreen(CraftingContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
		this.xSize = 176;
		this.ySize = 166;
		for(int i = 0; i < 4; ++i)
			((ListenerSlot)screenContainer.getSlot(i)).setListener(() -> init());
	}

	@Override
	protected void init() {
		super.init();
		
		for(Widget w : this.buttons)
			w.active = false;
		this.buttons.clear();
		List<TableRecipe> recipe = this.getRecipes();
		if(recipe != null) {
			int x = (this.width / 2) - 29, y = (this.height / 2) - 71;
			int index = 0;
			for(TableRecipe rec : recipe) {
				this.addButton(new ItemButton(x + ((index % 3) * 20), y + (index / 3) * 20, rec.getResult(), but -> {
					Network.sendToServer(new CraftingPacket(rec.getKey(), container.pos));
				}));
				++index;
			}
		}
		
	}
	
	public List<TableRecipe> getRecipes() {
		List<TableRecipe> list = Lists.newArrayList();
		for(TableRecipe rec : Recipes.RECIPES) {
			if(rec.matches(
				this.container.getSlot(0).getStack(),
				this.container.getSlot(1).getStack(),
				this.container.getSlot(2).getStack(),
				this.container.getSlot(3).getStack()
				))
			{
				list.add(rec);
			}
		}
		return list;
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		this.minecraft.getTextureManager().bindTexture(TEXTURE);
		
		this.blit(width / 2 - this.xSize / 2, height / 2 - this.ySize / 2, 0, 0, this.xSize, this.ySize);
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

}
