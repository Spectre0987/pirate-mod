package net.pirates.mod.client.guis.widgets;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;

public class ItemButton extends Button{

	private final ItemStack stack;
	
	public ItemButton(int width, int height, ItemStack stack, IPressable p_i51141_6_) {
		super(width, height, 20, 20, "", p_i51141_6_);
		this.stack = stack;
	}

	@Override
	public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_) {
		super.renderButton(p_renderButton_1_, p_renderButton_2_, p_renderButton_3_);
		
		RenderHelper.enableGUIStandardItemLighting();
		Minecraft.getInstance().getItemRenderer().renderItemIntoGUI(stack, this.x + 2, this.y + 2);
		RenderHelper.enableStandardItemLighting();
		
	}

}
