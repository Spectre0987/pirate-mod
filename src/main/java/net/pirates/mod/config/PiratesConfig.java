package net.pirates.mod.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.pirates.mod.Pirates;

@EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PiratesConfig {

	public static ForgeConfigSpec COMMON = setupConfig();
	
	public static ForgeConfigSpec.IntValue SHIP_CHANCE;
	
	@SubscribeEvent
	public static void load(ModConfig.Loading event) {
		
	}
	
	public static ForgeConfigSpec setupConfig() {
		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
		
		builder.push("world_gen");
		
		SHIP_CHANCE = builder.comment("Chance per chunk a ship will generate (1 out of X chunks)")
			.defineInRange("chance", 1000, 0, 100000);
		
		builder.pop();
		
		return builder.build();
	}
}
