package net.pirates.mod.datagen;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.pirates.mod.Pirates;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class DataGen {

	
	@SubscribeEvent
	public static void datagen(GatherDataEvent event) {
		event.getGenerator().addProvider(new PirateItemGen(event.getGenerator(), event.getExistingFileHelper()));
		event.getGenerator().addProvider(new PirateBlockGenerator(event.getGenerator(), event.getExistingFileHelper()));
		event.getGenerator().addProvider(new PirateBlockstateGen(event.getGenerator()));
 		event.getGenerator().addProvider(new PirateRecipeGen(event.getGenerator()));
 		event.getGenerator().addProvider(new PirateLootTableGen(event.getGenerator()));
	}
}
