package net.pirates.mod.datagen;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

public class TableRecipeBuilder {

	private ItemStack result;
	private List<Ingredient> inged = Lists.newArrayList();
	
	public TableRecipeBuilder(ItemStack stack) {
		this.result = stack;
	}
	
	protected TableRecipeBuilder() {}
	
	public TableRecipeBuilder addIngredient(Ingredient ing) {
		this.inged.add(ing);
		return this;
	}
	
	public JsonObject serialize() {
		JsonObject root = new JsonObject();
		
		JsonArray ingred = new JsonArray();
		for(Ingredient ing : this.inged) {
			ingred.add(ing.serialize());
		}
		root.add("ingredients", ingred);
		
		JsonObject result = new JsonObject();
		result.add("item", new JsonPrimitive(this.result.getItem().getRegistryName().toString()));
		result.add("count", new JsonPrimitive(this.result.getCount()));
		root.add("result", result);
		
		return root;
	}
	
}
