package net.pirates.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.pirates.mod.Pirates;

public class PirateLootTableGen implements IDataProvider {

	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator generator;
	
	public PirateLootTableGen(DataGenerator gen) {
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		Path base = this.generator.getOutputFolder();
		
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().contentEquals(Pirates.MODID)) {
				IDataProvider.save(GSON, cache, this.createSelfTable(block.getRegistryName().toString()), getPath(base, block));
			}
		}
		
	}

	@Override
	public String getName() {
		return "Pirate Mod Loot Gen";
	}
	
	public static Path getPath(Path base, Block block) {
		ResourceLocation key = block.getRegistryName();
		return base.resolve("data/" + key.getNamespace() + "/loot_tables/blocks/" + key.getPath() + ".json");
	}
	
	public JsonElement createSelfTable(String name) {
		JsonObject root = new JsonObject();
		
		root.add("type", new JsonPrimitive("minecraft:block"));
		
		JsonArray pools = new JsonArray();
		
		JsonObject first = new JsonObject();
		
		first.add("rolls", new JsonPrimitive(1));
		
		JsonArray entry = new JsonArray();
		JsonObject type = new JsonObject();
		type.add("type", new JsonPrimitive("item"));
		type.add("name", new JsonPrimitive(name));
		entry.add(type);
		first.add("entries", entry);
		
		pools.add(first);
		
		root.add("pools", pools);
		
		return root;
	}

}
