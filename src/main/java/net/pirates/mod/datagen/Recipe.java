package net.pirates.mod.datagen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.item.Item;

public class Recipe {
	
	private boolean shaped;
	private List<String> pattern = new ArrayList<String>();
	private HashMap<Character, Item> keys = new HashMap<Character, Item>();
	private Item result;
	
	public Recipe(boolean shaped) {
		this.shaped = shaped;
	}
	
	public Recipe addGridLine(String line) {
		this.pattern.add(line);
		return this;
	}
	
	public Recipe addIngred(char key, Item item) {
		this.keys.put(key, item);
		return this;
	}
	
	public Recipe addResult(Item item) {
		this.result = item;
		return this;
	}
	
	public JsonObject serialize() {
		JsonObject root = new JsonObject();
		
		root.add("type", new JsonPrimitive("minecraft:crafting_shaped"));
		
		JsonArray patternObj = new JsonArray();
		for(String s : this.pattern)
			patternObj.add(s);
		
		root.add("pattern", patternObj);
		
		JsonObject key = new JsonObject();
		for(Entry<Character, Item> entry : this.keys.entrySet()) {
			JsonObject obj = new JsonObject();
			obj.add("item", new JsonPrimitive(entry.getValue().getRegistryName().toString()));
			key.add(entry.getKey() + "", obj);
		}
		root.add("key", key);
		
		JsonObject result = new JsonObject();
		result.add("item", new JsonPrimitive(this.result.getRegistryName().toString()));
		root.add("result", result);
		
		return root;
	}

}
