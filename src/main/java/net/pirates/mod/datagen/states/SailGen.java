package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.state.IProperty;
import net.pirates.mod.blocks.MainSailBlock;
import net.pirates.mod.blocks.MainSailBlock.SailType;

public class SailGen implements StateSerializer {

	@Override
	public boolean isForProp(IProperty<?> prop) {
		return prop == MainSailBlock.TYPE;
	}

	@Override
	public String getName(Object val) {
		return "type=" + ((SailType)val).getName();
	}

	@Override
	public void serialize(JsonObject block, Object val) {
		SailType type = (SailType)val;
		switch(type) {
			case TOP: {
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_top_black"));
				break;
			}
			case MIDDLE: {
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_black"));
				break;
			}
			case BOTTOM: {
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_bottom_black"));
				break;
			}
			case TOP_CONNECT:{
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_top_connect_black"));
				break;
			}
			case MIDDLE_CONNECT:{
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_connect_black"));
				break;
			}
			case BOTTOM_CONNECT:{
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_bottom_connect_black"));
				break;
			}
			case RIPPED:{
				block.add("model", new JsonPrimitive("pirates:block/sails/sail_panel_ripped_black"));
				break;
			}
		}
	}

}
