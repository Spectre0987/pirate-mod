package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;

import net.minecraft.state.IProperty;

public interface StateSerializer {

	
	boolean isForProp(IProperty<?> prop);
	String getName(Object val);
	void serialize(JsonObject block, Object val);
}
