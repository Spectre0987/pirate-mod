package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.state.IProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.pirates.mod.helper.Helper;

public class FacingSerializer implements StateSerializer{

	@Override
	public boolean isForProp(IProperty<?> prop) {
		return prop == BlockStateProperties.HORIZONTAL_FACING;
	}

	@Override
	public String getName(Object val) {
		return "facing=" + ((Direction)val).getName2();
	}

	@Override
	public void serialize(JsonObject block, Object val) {
		block.add("y", new JsonPrimitive(Helper.getAngleForFacing((Direction)val)));
	}

}
