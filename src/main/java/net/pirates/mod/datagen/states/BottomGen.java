package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.state.IProperty;
import net.pirates.mod.blocks.RatlinesBlock;

public class BottomGen implements StateSerializer {

	@Override
	public boolean isForProp(IProperty<?> prop) {
		return prop == RatlinesBlock.BOTTOM;
	}

	@Override
	public String getName(Object val) {
		return "bottom=" + ((Boolean)val);
	}

	@Override
	public void serialize(JsonObject block, Object val) {
		if(((Boolean)val)) {
			block.add("model", new JsonPrimitive("pirates:block/ratlines_base"));
		}
		else block.add("model", new JsonPrimitive("pirates:block/ratlines"));
	}

}
