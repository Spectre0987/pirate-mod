package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.state.IProperty;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.RailingBlock;

public class EndSerializer implements StateSerializer {

	@Override
	public boolean isForProp(IProperty<?> prop) {
		return prop == RailingBlock.END;
	}

	@Override
	public String getName(Object val) {
		return "end=" + (Boolean)val;
	}

	@Override
	public void serialize(JsonObject block, Object val) {
		Boolean bool = (Boolean)val;
		
		if(bool)
			block.add("model", new JsonPrimitive(Pirates.MODID + ":block/railings_end"));
		else block.add("model", new JsonPrimitive(Pirates.MODID + ":block/railings"));
	}

}
