package net.pirates.mod.datagen.states;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.state.IProperty;
import net.minecraft.util.Direction.Axis;

public class AxisGen implements StateSerializer{

	@Override
	public boolean isForProp(IProperty<?> prop) {
		return RotatedPillarBlock.AXIS == prop;
	}

	@Override
	public String getName(Object val) {
		return "axis=" + ((Axis)val).getName2();
	}

	@Override
	public void serialize(JsonObject block, Object val) {
		
		Axis ax = (Axis)val;
		
		if(ax == Axis.X) {
			block.add("x", new JsonPrimitive(90));
			block.add("y", new JsonPrimitive(90));
		}
		else if(ax == Axis.Z) {
			block.add("x", new JsonPrimitive(90));
		}

	}

}
