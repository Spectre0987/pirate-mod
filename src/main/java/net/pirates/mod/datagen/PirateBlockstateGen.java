package net.pirates.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.state.IProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.PBlocks;
import net.pirates.mod.datagen.states.AxisGen;
import net.pirates.mod.datagen.states.BottomGen;
import net.pirates.mod.datagen.states.EndSerializer;
import net.pirates.mod.datagen.states.FacingSerializer;
import net.pirates.mod.datagen.states.SailGen;
import net.pirates.mod.datagen.states.StateSerializer;

public class PirateBlockstateGen implements IDataProvider {

	public static final StateSerializer[] SERIALIZERS = {
			new FacingSerializer(),
			new EndSerializer(),
			new AxisGen(),
			new SailGen(),
			new BottomGen()};
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	public DataGenerator generator;
	
	public PirateBlockstateGen(DataGenerator gen) {
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache arg0) throws IOException {

		Path base = this.generator.getOutputFolder();
		
		for(WoodType type : WoodType.TYPES) {
			this.generateBasic(base, arg0,
					type.logsBlock.get(),
					type.planksBlock.get(),
					type.rails.get());
		}
		
		this.generateBasic(base, arg0,
			PBlocks.SLING,
			PBlocks.CRAFTING_TABLE,
			PBlocks.HATCH_SOLID,
			PBlocks.MAINSAIL,
			PBlocks.SHIPS_WHEEL,
			PBlocks.CROWS_NEST,
			PBlocks.RATLINES
		);
		
	}

	@Override
	public String getName() {
		return "Pirate Blockstate Generator";
	}
	
	public void generateBasic(Path base, DirectoryCache cache, Block... blocks) throws IOException {
		
		for(Block block : blocks) {
			IDataProvider.save(GSON, cache, this.createBasic(block.getRegistryName().getPath(), block.getStateContainer()), getPath(base, block));
		}
		
	}
	
	public static Path getPath(Path base, Block b) {
		ResourceLocation key = b.getRegistryName();
		return base.resolve("assets/" + key.getNamespace() + "/blockstates/" + key.getPath() + ".json");
	}
	
	@SuppressWarnings("unchecked")
	public JsonObject createBasic(String model, StateContainer<Block, BlockState> container) {
		JsonObject root = new JsonObject();
		
		root.add("variants", this.test(model, container));
		return root;
	}
	
	public JsonObject test(String model, StateContainer<Block, BlockState> container) {
		JsonObject variant = new JsonObject();
		Map<String, JsonObject> stuff = new HashMap<>();
		
		List<IProperty<?>> generated = new ArrayList<>();
		
		for(IProperty<?> prop : container.getProperties()) {
			
			StateSerializer ser = this.getSerialzier(prop);
			if(ser == null || generated.contains(prop))
				continue;
			
			for(Object val : prop.getAllowedValues()) {
				
				//Others to combine
				if(container.getProperties().size() > 1) {
					for(IProperty<?> p : container.getProperties()) {
						StateSerializer s = this.getSerialzier(p);
					
						if(s == null || p == prop)
							continue;
						
						for(Object o : p.getAllowedValues()) {
							JsonObject jo = new JsonObject();
							ser.serialize(jo, val);
							s.serialize(jo, o);
							stuff.put(ser.getName(val) + "," + s.getName(o), jo);
						}
						
						generated.add(p);
						
					}
				}
				else {
					JsonObject jo = new JsonObject();
					ser.serialize(jo, val);
					stuff.put(ser.getName(val), jo);
				}
			}
		}
		
		//Empty
		if(container.getProperties().isEmpty()) {
			JsonObject obj = new JsonObject();
			obj.add("model", new JsonPrimitive(model));
			stuff.put("", obj);
		}
		//If only facing
		if(container.getProperties().size() == 1 && container.getProperties().contains(BlockStateProperties.HORIZONTAL_FACING)) {
			
		}
		
		for(Entry<String, JsonObject> entry : stuff.entrySet()) {
				if(!entry.getValue().has("model"))
					entry.getValue().add("model", new JsonPrimitive(Pirates.MODID + ":block/" + model));
				variant.add(entry.getKey(), entry.getValue());
		}
		
		return variant;
	}
	
	@Nullable
	public StateSerializer getSerialzier(IProperty<?> prop){
		for(StateSerializer ser : SERIALIZERS) {
			if(ser.isForProp(prop))
				return ser;
		}
		return null;
	}
	
}
