package net.pirates.mod.datagen;

import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.PBlocks;

public class WoodType {

	public static final WoodType GHASTLY = new WoodType("ghastly",
			() -> PBlocks.GHASTLY_PLANKS,
			() -> PBlocks.GHASTLY_LOGS,
			() -> PBlocks.GHASTLY_RAILINGS);
	
	public static final WoodType[] TYPES = new WoodType[] {GHASTLY};
	
	public ResourceLocation planks;
	public ResourceLocation logs;
	public ResourceLocation logsTop;
	public ResourceLocation stripped;
	
	public Supplier<Block> planksBlock;
	public Supplier<Block> logsBlock;
	public Supplier<Block> rails;
	
	public WoodType(String baseName, Supplier<Block> planks, Supplier<Block> logs, Supplier<Block> rails) {
		this.planks = buildResourceLocation(baseName + "_planks");
		this.logs = buildResourceLocation(baseName + "_log");
		this.logsTop = buildResourceLocation(baseName + "_log_top");
		this.stripped = buildResourceLocation(baseName + "_stripped");
		
		this.planksBlock = planks;
		this.logsBlock = logs;
		this.rails = rails;
	}
	
	public static ResourceLocation buildResourceLocation(String name) {
		return new ResourceLocation(Pirates.MODID, "block/wood/" + name);
	}

}
