package net.pirates.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraftforge.common.Tags;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.PBlocks;
import net.pirates.mod.items.PItems;

public class PirateRecipeGen implements IDataProvider {

	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator generator;
	
	public PirateRecipeGen(DataGenerator gen) {
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		Path base = generator.getOutputFolder();
		
		//generate(new Recipe(true).addGridLine(" t ").addIngred('t', PItems.coin).addResult(PItems.hardtack), "coin", arg0, base);
		
		generateCustom(
				new TableRecipeBuilder(new ItemStack(PItems.slowmatch))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS))
				.addIngredient(Ingredient.fromTag(Tags.Items.STRING))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS))
				.addIngredient(Ingredient.fromTag(Tags.Items.STRING)),
				"slowmatch", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.CHAIN, 16))
				.addIngredient(Ingredient.fromTag(Tags.Items.INGOTS_IRON)), "chain", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PItems.coin, 8))
				.addIngredient(Ingredient.fromTag(Tags.Items.INGOTS_GOLD)), "coin", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PItems.telescope))
				.addIngredient(Ingredient.fromTag(Tags.Items.INGOTS_GOLD))
				.addIngredient(Ingredient.fromTag(Tags.Items.LEATHER))
				.addIngredient(Ingredient.fromTag(Tags.Items.INGOTS_GOLD))
				.addIngredient(Ingredient.fromTag(Tags.Items.GLASS_PANES)), "telescope", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PItems.cartridge, 9))
				.addIngredient(Ingredient.fromTag(Tags.Items.INGOTS_IRON))
				.addIngredient(Ingredient.fromTag(Tags.Items.GUNPOWDER))
				.addIngredient(Ingredient.fromItems(Items.PAPER)),
				"ammo", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.BOWSPRIT))
				.addIngredient(Ingredient.fromTag(ItemTags.LOGS)), "bowsprit", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.BOWSPRIT_SPAR, 4))
				.addIngredient(Ingredient.fromTag(ItemTags.LOGS)), "bowsprit_spar", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.SLING))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS))
				.addIngredient(Ingredient.fromItems(PBlocks.CHAIN.asItem()))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS))
				.addIngredient(Ingredient.fromItems(PBlocks.CHAIN.asItem())),
				"boat_sling", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.SHIPS_WHEEL, 1))
				.addIngredient(Ingredient.fromTag(ItemTags.LOGS))
				.addIngredient(Ingredient.fromItems(Items.STRING))
				.addIngredient(Ingredient.fromItems(Items.STRING))
				.addIngredient(Ingredient.fromItems(Items.STICK)), "ships_wheel", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.MAINSAIL, 1))
				.addIngredient(Ingredient.fromItems(Blocks.BLACK_WOOL))
				.addIngredient(Ingredient.fromItems(Blocks.BLACK_WOOL))
				.addIngredient(Ingredient.fromItems(
						Blocks.STRIPPED_BIRCH_LOG,
						Blocks.STRIPPED_ACACIA_LOG,
						Blocks.STRIPPED_DARK_OAK_LOG,
						Blocks.STRIPPED_JUNGLE_LOG,
						Blocks.STRIPPED_OAK_LOG,
						Blocks.STRIPPED_SPRUCE_LOG)), "sail_black", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.CROWS_NEST))
				.addIngredient(Ingredient.fromTag(ItemTags.BANNERS))
				.addIngredient(Ingredient.fromItems(
						Blocks.STRIPPED_BIRCH_LOG,
						Blocks.STRIPPED_ACACIA_LOG,
						Blocks.STRIPPED_DARK_OAK_LOG,
						Blocks.STRIPPED_JUNGLE_LOG,
						Blocks.STRIPPED_OAK_LOG,
						Blocks.STRIPPED_SPRUCE_LOG))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS))
				.addIngredient(Ingredient.fromTag(ItemTags.PLANKS)), "crows_nest", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.RATLINES, 8))
				.addIngredient(Ingredient.fromTag(Tags.Items.STRING))
				.addIngredient(Ingredient.fromTag(Tags.Items.STRING))
				.addIngredient(Ingredient.fromItems(Items.STICK))
				.addIngredient(Ingredient.fromItems(Items.STICK)), "ratlines", cache, base);
		
		generateCustom(new TableRecipeBuilder(new ItemStack(PBlocks.HATCH_SOLID))
				.addIngredient(Ingredient.fromItems(Blocks.STRIPPED_OAK_LOG)), "sold_hatch", cache, base);
		
	}
	
	public static Path getPath(Path base, String name) {
		return base.resolve("data/" + Pirates.MODID + "/recipes/" + name + ".json");
	}
	
	public static void generate(Recipe rec, String name, DirectoryCache cache, Path base) throws IOException {
		IDataProvider.save(GSON, cache, rec.serialize(), getPath(base, name));
	}
	
	public static void generateCustom(TableRecipeBuilder rec, String name, DirectoryCache cache, Path base) {
		try{
			IDataProvider.save(GSON, cache, rec.serialize(), getCustomPath(base, name));
		}
		catch(Exception e) {
			Pirates.LOGGER.catching(e);
		}
	}
	
	public static Path getCustomPath(Path base, String name) {
		return base.resolve("data/" + Pirates.MODID + "/pirates_crafting/" + name + ".json");
	}

	@Override
	public String getName() {
		return "Pirate Recipe Generator";
	}

}
