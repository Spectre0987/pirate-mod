package net.pirates.mod.datagen;


import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.pirates.mod.Pirates;

public class PirateBlockGenerator extends BlockModelProvider{
	
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	public PirateBlockGenerator(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, Pirates.MODID, existingFileHelper);
	}

	@Override
	public String getName() {
		return "Pirate Block Model Generator";
	}

	@Override
	protected void registerModels() {
		try {
			Path base = this.generator.getOutputFolder();
			
			this.generateForWoodType(base, cache, WoodType.GHASTLY);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void generateForWoodType(Path base, DirectoryCache cache, WoodType type) throws IOException {
		this.generateCubeAll(base, cache, type.planksBlock.get(), type.planks);
		this.generateModelFile(base, cache, type.logsBlock.get(), this.createPillarBlock(type.logsTop, type.logs));
		this.generateModelFile(base, cache, type.rails.get(), this.createRailingBlock(type.logs, type.stripped));
		
	}
	
	public void generateCubeAll(Path base, DirectoryCache cache, Block block, ResourceLocation texture) throws IOException {
		IDataProvider.save(GSON, cache, this.createCubeAll(texture), getPath(base, block));
	}
	
	public void generateModelFile(Path path, DirectoryCache cache, Block block, JsonObject serialized) throws IOException {
		IDataProvider.save(GSON, cache, serialized, getPath(path, block));
	}
	
	public static Path getPath(Path base, Block block) {
		return base.resolve("assets/" + block.getRegistryName().getNamespace() + "/models/block/" + block.getRegistryName().getPath() + ".json");
	}
	
	public JsonElement createCubeAll(ResourceLocation texture) {
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive("block/cube_all"));
		
		JsonObject textures = new JsonObject();
		textures.add("all", new JsonPrimitive(texture.toString()));
		
		root.add("textures", textures);
		return root;
	}
	
	public JsonObject createRailingBlock(ResourceLocation logs, ResourceLocation stripped) {
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive(Pirates.MODID + ":block/railings"));
		
		JsonObject textures = new JsonObject();
		textures.add("0", new JsonPrimitive(logs.toString()));
		textures.add("3", new JsonPrimitive(stripped.toString()));
		textures.add("particles", new JsonPrimitive(logs.toString()));
		
		root.add("textures", textures);
		return root;
	}
	
	public JsonObject createRailingBlockEnd(ResourceLocation logs, ResourceLocation stripped) {
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive(Pirates.MODID + ":block/railings_end"));
		
		JsonObject textures = new JsonObject();
		textures.add("0", new JsonPrimitive(logs.toString()));
		textures.add("3", new JsonPrimitive(stripped.toString()));
		textures.add("particles", new JsonPrimitive(logs.toString()));
		
		root.add("textures", textures);
		return root;
	}
	
	public JsonObject createPillarBlock(ResourceLocation end, ResourceLocation side) {
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive("block/cube_column"));
		
		JsonObject textures = new JsonObject();
		textures.add("end", new JsonPrimitive(end.toString()));
		textures.add("side", new JsonPrimitive(side.toString()));
		
		root.add("textures", textures);
		return root;
	}
}