package net.pirates.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.registries.ForgeRegistries;
import net.pirates.mod.Pirates;
import net.pirates.mod.blocks.PBlocks;

public class PirateItemGen extends ItemModelProvider {

	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public PirateItemGen(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, Pirates.MODID, existingFileHelper);
	}

	@Override
	public String getName() {
		return "Pirate Item Model Generator";
	}

	@Override
	protected void registerModels(){
		try {
			Path base = this.generator.getOutputFolder();
			this.generateBlockItemModel(cache, base,
					PBlocks.GHASTLY_PLANKS
			);
			
			for(WoodType type : WoodType.TYPES) {
				this.generateBlockItemModel(cache, base, type.logsBlock.get(), type.planksBlock.get(), type.rails.get());
			}
			
			for(Block b : ForgeRegistries.BLOCKS) {
				if(b.getRegistryName().getNamespace().contentEquals(Pirates.MODID))
					this.generateBlockItemModel(cache, base, b);
			}
			
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void generateBlockItemModel(DirectoryCache cache, Path base, Block... blocks) throws IOException {
		for(Block block : blocks) {
			ResourceLocation key = block.getRegistryName();
			IDataProvider.save(GSON, cache, this.createParentModel(key.getNamespace() + ":block/" + key.getPath()), getPath(base, block));
		}
	}
	
	public static Path getPath(Path base, Block block) {
		return base.resolve("assets/" + block.getRegistryName().getNamespace() + "/models/item/" + block.getRegistryName().getPath() + ".json");
	}
	
	private JsonObject createParentModel(String blockModel) {
		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive(blockModel));
		return root;
	}
	
}
