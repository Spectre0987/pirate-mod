package net.pirates.mod.ship;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.pirates.mod.helper.BlockStore;

public class BlockShell implements INBTSerializable<CompoundNBT>{

	public Map<BlockPos, BlockStore> blocks = Maps.newHashMap();
	private BlockPos offset;
	
	public BlockShell(BlockPos offset) {
		this.offset = offset;
	}
	
	public void putBlocks(Map<BlockPos, BlockStore> blocks) {
		this.blocks.clear();
		this.blocks.putAll(blocks);
	}
	
	public void setOffset(BlockPos pos) {
		this.offset = pos;
	}
	
	public BlockPos getOffset() {
		return this.offset;
	}
	
	public void encode(PacketBuffer buf) {
		buf.writeBlockPos(offset);
		buf.writeInt(this.blocks.size());
		for(Entry<BlockPos, BlockStore> entry : this.blocks.entrySet()) {
			buf.writeLong(entry.getKey().toLong());
			entry.getValue().encode(buf);
		}
	}
	
	public void decode(PacketBuffer buf) {
		
		this.blocks.clear();
		
		this.offset = buf.readBlockPos();
		
		int size = buf.readInt();
		for(int i = 0; i < size; ++i) {
			blocks.put(BlockPos.fromLong(buf.readLong()), new BlockStore(buf));
		}
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		tag.putLong("offset", this.offset.toLong());
		
		ListNBT list = new ListNBT();
		
		for(Entry<BlockPos, BlockStore> entry : this.blocks.entrySet()) {
			CompoundNBT blockTag = new CompoundNBT();
			blockTag.putLong("pos", entry.getKey().toLong());
			blockTag.put("data", entry.getValue().serializeNBT());
			list.add(blockTag);
		}
		
		tag.put("data", list);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.offset = BlockPos.fromLong(tag.getLong("offset"));
		
		this.blocks.clear();
		
		ListNBT list = tag.getList("data", NBT.TAG_COMPOUND);
		for(INBT n : list) {
			CompoundNBT data = (CompoundNBT)n;
			blocks.put(BlockPos.fromLong(data.getLong("pos")), new BlockStore(data.getCompound("data")));
		}
	}
	
}
