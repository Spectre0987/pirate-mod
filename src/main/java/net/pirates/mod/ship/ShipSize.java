package net.pirates.mod.ship;

import java.util.Set;

import net.minecraft.entity.EntitySize;
import net.minecraft.util.math.BlockPos;

public class ShipSize {

	EntitySize size;
	
	public ShipSize(Set<BlockPos> positions) {
		
		int minX = Integer.MAX_VALUE;
		int maxX = -Integer.MAX_VALUE;
		
		int minY = Integer.MAX_VALUE;
		int maxY = -Integer.MAX_VALUE;
		
		int minZ = Integer.MAX_VALUE;
		int maxZ = -Integer.MAX_VALUE;
		
		for(BlockPos pos : positions) {
			
			if(pos.getX() < minX)
				minX = pos.getX();
			if(pos.getX() > maxX)
				maxX = pos.getX();
			
			if(pos.getY() < minY)
				minY = pos.getY();
			if(pos.getY() > maxY)
				maxY = pos.getY();
			
			if(pos.getZ() < minZ)
				minZ = pos.getZ();
			if(pos.getZ() > maxZ)
				maxZ = pos.getZ();
		}
		
		size = EntitySize.fixed(maxX - minX, maxY - minY);
	}
	
	public EntitySize getSize() {
		return this.size;
	}
}
