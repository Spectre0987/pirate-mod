package net.pirates.mod.ship;

public interface IHaveShell {

	void setShell(BlockShell shell);
	BlockShell getShell();
}
