package net.pirates.mod.net.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkEvent;
import net.pirates.mod.cap.Capabilities;

public class UpdatePlayerMessage {

	int playerID;
	CompoundNBT data;
	
	public UpdatePlayerMessage(int player, CompoundNBT tag) {
		this.playerID = player;
		this.data = tag;
	}
	
	public static void encode(UpdatePlayerMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.playerID);
		buf.writeCompoundTag(mes.data);
	}
	
	public static UpdatePlayerMessage decode(PacketBuffer buf) {
		return new UpdatePlayerMessage(buf.readInt(), buf.readCompoundTag());
	}
	
	public static void handle(UpdatePlayerMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> handleClient(mes));
		cont.get().setPacketHandled(true);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void handleClient(UpdatePlayerMessage mes) {
		Entity ent = Minecraft.getInstance().world.getEntityByID(mes.playerID);
		if(ent instanceof PlayerEntity) {
			ent.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.deserializeNBT(mes.data));
		}
	}
}
