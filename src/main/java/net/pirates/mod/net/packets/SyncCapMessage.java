package net.pirates.mod.net.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.pirates.mod.cap.Capabilities;

public class SyncCapMessage {
	
	public CompoundNBT tag;
	
	public SyncCapMessage(CompoundNBT tag) {
		this.tag = tag;
	}
	
	public static void encode(SyncCapMessage mes, PacketBuffer buf) {
		buf.writeCompoundTag(mes.tag);
	}

	public static SyncCapMessage decode(PacketBuffer buf) {
		return new SyncCapMessage(buf.readCompoundTag());
	}
	
	public static void handle(SyncCapMessage mes, Supplier<NetworkEvent.Context> con) {
		Minecraft.getInstance().enqueue(() -> {
			ItemStack stack = Minecraft.getInstance().player.getHeldItemMainhand();
			stack.getCapability(Capabilities.GUN).ifPresent(cap -> cap.deserializeNBT(mes.tag));
		});
		con.get().setPacketHandled(true);
	}
}
