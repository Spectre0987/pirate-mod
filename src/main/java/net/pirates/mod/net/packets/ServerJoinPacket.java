package net.pirates.mod.net.packets;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.pirates.mod.net.Packet;
import net.pirates.mod.recipes.Recipes;
import net.pirates.mod.recipes.TableRecipe;

public class ServerJoinPacket extends Packet<ServerJoinPacket> {

	List<TableRecipe> recipes = Lists.newArrayList();
	
	public ServerJoinPacket(List<TableRecipe> recipes) {
		this.recipes.clear();
		this.recipes.addAll(recipes);
	}

	@Override
	public void encode(ServerJoinPacket mes, PacketBuffer buf) {
		buf.writeInt(mes.recipes.size());
		for(TableRecipe rec : mes.recipes) {
			rec.encode(buf);
		}
	}

	@Override
	public ServerJoinPacket decode(PacketBuffer buf) {
		int size = buf.readInt();
		List<TableRecipe> list = Lists.newArrayList();
		for(int i = 0; i < size; i++) {
			list.add(TableRecipe.decode(buf));
		}
		return new ServerJoinPacket(list);
	}

	@Override
	public void handle(ServerJoinPacket mes, Context cont) {
		cont.enqueueWork(() -> {
			Recipes.RECIPES.clear();
			Recipes.RECIPES.addAll(mes.recipes);
			Recipes.RECIPES.sort(Recipes.SORTER);
		});
		cont.setPacketHandled(true);
	}
}
