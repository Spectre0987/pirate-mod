package net.pirates.mod.net;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

public abstract class Packet<T extends Packet<T>>{

	public abstract void encode(T mes, PacketBuffer buf);
	public abstract T decode(PacketBuffer buf);
	public abstract void handle(T mes, NetworkEvent.Context cont);
}
