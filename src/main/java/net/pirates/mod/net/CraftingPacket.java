package net.pirates.mod.net;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.pirates.mod.Pirates;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.recipes.Recipes;
import net.pirates.mod.recipes.TableRecipe;
import net.pirates.mod.tileentites.CraftingTableTile;

public class CraftingPacket extends Packet<CraftingPacket> {

	ResourceLocation key;
	BlockPos pos;
	
	public CraftingPacket(ResourceLocation key, BlockPos pos) {
		this.key = key;
		this.pos = pos;
	}
	
	@Override
	public void encode(CraftingPacket mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.key);
		buf.writeBlockPos(mes.pos);
	}

	@Override
	public CraftingPacket decode(PacketBuffer buf) {
		return new CraftingPacket(buf.readResourceLocation(), buf.readBlockPos());
	}

	@Override
	public void handle(CraftingPacket mes, Context cont) {
		cont.enqueueWork(() -> {
			try {
				TileEntity te = cont.getSender().world.getTileEntity(mes.pos);
				TableRecipe rec = Recipes.getByResourceLocation(mes.key);
				if(te instanceof CraftingTableTile) {
					te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(inv -> {
						if(inv instanceof ItemStackHandler) {
							for(int i = 0; i < 5; ++i) {
								inv.extractItem(i, 1, false);
							}
							ItemStack stack = ((ItemStackHandler)inv).insertItem(4, rec.getResult(), false);
							if(!stack.isEmpty())
								Helper.dropItemAtEntity(cont.getSender(), stack);
						}
					});
					
				}
			}
			catch(Exception e) {
				Pirates.LOGGER.catching(e);
			}
		});
		cont.setPacketHandled(true);
	}



}
