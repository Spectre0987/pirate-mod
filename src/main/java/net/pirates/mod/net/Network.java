package net.pirates.mod.net;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.pirates.mod.Pirates;
import net.pirates.mod.net.packets.ServerJoinPacket;
import net.pirates.mod.net.packets.SyncCapMessage;
import net.pirates.mod.net.packets.UpdatePlayerMessage;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class Network {

	public static String VERS = "1";
	public static int indexID = 0;
	public static SimpleChannel CHANNEL = NetworkRegistry.newSimpleChannel(new ResourceLocation(Pirates.MODID, "main"),
			() -> VERS, VERS::equals, VERS::equals);
	
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		CHANNEL.registerMessage(indexID++, SyncCapMessage.class, SyncCapMessage::encode, SyncCapMessage::decode, SyncCapMessage::handle);
		registerPacket(new ServerJoinPacket(Lists.newArrayList()));
		registerPacket(new CraftingPacket(new ResourceLocation(""), BlockPos.ZERO));
		CHANNEL.registerMessage(++indexID, UpdatePlayerMessage.class, UpdatePlayerMessage::encode, UpdatePlayerMessage::decode, UpdatePlayerMessage::handle);
	}
	
	@SuppressWarnings("unchecked")
	public static <P extends Packet<P>> void registerPacket(Packet<P> p) {
		CHANNEL.registerMessage(++indexID, (Class<P>)p.getClass(), p::encode, p::decode,
				(mes, cont) -> p.handle(mes, cont.get()));
	}

	public static void sendToPlayer(ServerPlayerEntity player, Packet<?> packet) {
		CHANNEL.sendTo(packet, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
	}

	public static void sendToServer(Packet<?> p) {
		CHANNEL.sendToServer(p);
	}

	//Helpers
	public static void writeVec3d(Vec3d vec, PacketBuffer buf) {
		buf.writeDouble(vec.x);
		buf.writeDouble(vec.y);
		buf.writeDouble(vec.z);
	}
	
	public static Vec3d readVec3d(PacketBuffer buf) {
		double x = buf.readDouble();
		double y = buf.readDouble();
		double z = buf.readDouble();
		return new Vec3d(x, y, z);
	}
	
}
