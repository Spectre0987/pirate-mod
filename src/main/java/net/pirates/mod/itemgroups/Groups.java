package net.pirates.mod.itemgroups;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.pirates.mod.Pirates;
import net.pirates.mod.items.PItems;

public class Groups {
	
	public static ItemGroup PIRATE = new ItemGroup(Pirates.MODID + ":main") {

		@Override
		public ItemStack createIcon() {
			return new ItemStack(PItems.flintlock);
		}
		
	};

	
	/**
	 * An empty function serving as an entry point
	 */
	public static void init() {}
}
