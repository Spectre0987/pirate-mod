package net.pirates.mod.sail;

public class SailHelper {
	
	public static double getSailEfficiency(float angle, Wind wind) {
		float realAngle = wind.getWindAngle() - angle;
		
		return realAngle;
	}
	
	public static enum EnumSailConfig{
		RUN,
		CLOSE_REACH,
		BROAD_REACH;
	}

}
