package net.pirates.mod.sail;

public class Wind {
	
	private float angle = 0;
	private int knots = 20;
	
	public float getWindAngle() {
		return this.angle;
	}
	
	public int getKnots() {
		return knots;
	}

}
