package net.pirates.mod;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.pirates.mod.cap.BagCapability;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.cap.GunCapability;
import net.pirates.mod.cap.IBagCap;
import net.pirates.mod.cap.IGun;
import net.pirates.mod.cap.IPlayerData;
import net.pirates.mod.cap.PlayerDataCapability;
import net.pirates.mod.config.PiratesConfig;
import net.pirates.mod.itemgroups.Groups;
import net.pirates.mod.proxy.ClientProxy;
import net.pirates.mod.proxy.IProxy;
import net.pirates.mod.proxy.ServerProxy;
import net.pirates.mod.tileentites.PTiles;

@Mod(Pirates.MODID)
public class Pirates {

	public static final String MODID = "pirates";
	
	public static IProxy PROXY;
	
	public static final Logger LOGGER = LogManager.getLogger(MODID);
	
	public Pirates() {
		Groups.init();
		
		DistExecutor.runForDist(() -> () -> PROXY = new ClientProxy(), () -> () -> PROXY = new ServerProxy());
		
		ModLoadingContext.get().registerConfig(Type.COMMON, PiratesConfig.COMMON, "pirates.toml");
		
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
		PTiles.TILES.register(bus);
		
	}
	
	@SubscribeEvent
	public static void setupEvent(FMLCommonSetupEvent event) {
		CapabilityManager.INSTANCE.register(IGun.class, new Capabilities.GunStorage(), GunCapability::new);
		CapabilityManager.INSTANCE.register(IBagCap.class, new Capabilities.BagStorage(), () -> new BagCapability(9, item -> true));
		CapabilityManager.INSTANCE.register(IPlayerData.class, new IPlayerData.Storage(), () -> new PlayerDataCapability(null));
	}
}
