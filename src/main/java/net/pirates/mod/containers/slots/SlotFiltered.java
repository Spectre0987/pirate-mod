package net.pirates.mod.containers.slots;

import java.util.function.Predicate;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotFiltered extends SlotItemHandler {

	private Predicate<ItemStack> test;
	
	public SlotFiltered(IItemHandler itemHandler, int index, int xPosition, int yPosition, Predicate<ItemStack> pred) {
		super(itemHandler, index, xPosition, yPosition);
		this.test = pred;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return test.test(stack);
	}

}
