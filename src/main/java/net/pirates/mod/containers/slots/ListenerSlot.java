package net.pirates.mod.containers.slots;

import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;


public class ListenerSlot extends SlotItemHandler{

	Runnable press;
	
	public ListenerSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public void onSlotChanged() {
		super.onSlotChanged();
		if(this.press != null)
			press.run();
	}
	
	public void setListener(Runnable press) {
		this.press = press;
	}

}
