package net.pirates.mod.containers;

import java.util.List;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.pirates.mod.containers.slots.ListenerSlot;
import net.pirates.mod.containers.slots.SlotFiltered;
import net.pirates.mod.helper.Helper;

public class CraftingContainer extends Container{

	public BlockPos pos = BlockPos.ZERO;
	
	public CraftingContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client constructor
	public CraftingContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		this(PContainers.CRAFTING, id);
		this.pos = buf.readBlockPos();
		inv.player.world.getTileEntity(pos).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(table -> {
			init(inv, (IItemHandlerModifiable)table);
		});
	}
	
	//Server
	public CraftingContainer(int id, PlayerInventory inv, IItemHandlerModifiable table) {
		this(PContainers.CRAFTING, id);
		init(inv, table);
	}
	
	public void init(PlayerInventory player, IItemHandlerModifiable table) {
		this.addSlot(new ListenerSlot(table, 0, 12, 31));
		this.addSlot(new ListenerSlot(table, 1, 30, 31));
		this.addSlot(new ListenerSlot(table, 2, 12, 49));
		this.addSlot(new ListenerSlot(table, 3, 30, 49));
		
		this.addSlot(new SlotFiltered(table, 4, 144, 57, item -> false));
		
		List<Slot> playerSlots = Helper.fillPlayerSlots(player, -19);
		
		for(Slot s : playerSlots) {
			this.addSlot(s);
		}
		
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(PlayerEntity player, int index) {
		if(this.getSlot(index).inventory == player.inventory) {
			for(int i = 0; i < 4; ++i) {
				if(this.getSlot(i).getStack().isEmpty()) {
					this.getSlot(i).putStack(this.getSlot(index).getStack());
					this.getSlot(index).putStack(ItemStack.EMPTY);
					return ItemStack.EMPTY;
				}
			}
		}
		else {
			Helper.dropItemAtEntity(player, this.getSlot(index).getStack());
			this.getSlot(index).putStack(ItemStack.EMPTY);
		}
		return ItemStack.EMPTY;
	}

}
