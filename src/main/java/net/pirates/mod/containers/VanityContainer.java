package net.pirates.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.SlotItemHandler;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.helper.Helper;

public class VanityContainer extends Container{

	//Common
	public VanityContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client 
	public VanityContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(PContainers.VANITY, id);
		this.init(inv, inv.player);
	}
	
	//Server
	public VanityContainer(int id, PlayerInventory inv, PlayerEntity player) {
		super(PContainers.VANITY, id);
		this.init(inv, player);
	}
	
	public void init(PlayerInventory inv, PlayerEntity player) {
		player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
			for(int i = 0; i < cap.getClothing().getSlots(); ++i) {
				this.addSlot(new SlotItemHandler(cap.getClothing(), i, 61, 8 + (i * 18)));
			}
		});
		
		for(Slot s : Helper.fillPlayerSlots(inv, -19)) {
			this.addSlot(s);
		}
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

}
