package net.pirates.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.SlotItemHandler;
import net.pirates.mod.cap.Capabilities;

public class BagContainer extends Container{
	
	public BagContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client
	public BagContainer(int id, PlayerInventory inv, PacketBuffer data){
		this(id, inv, data.readItemStack());
	}
	
	//Common
	public BagContainer(int id, PlayerInventory inv, ItemStack stack){
		super(PContainers.BAG, id);
		
		stack.getCapability(Capabilities.BAG).ifPresent(cap -> {
			int x = 62, y = 17;
			for(int i = 0; i < cap.getSlots(); ++i) {
				int row = i % 3;
				int col = i / 3;
				this.addSlot(new SlotItemHandler(cap, i, x + (col * 18), y + (row * 18)));
			}
		});
		
		int offset = -19;
		
		for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            this.addSlot(new Slot(inv, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + offset));
	         }
	      }

	      for(int i1 = 0; i1 < 9; ++i1) {
	         this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 161 + offset));
	      }
		
	}

	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		
		return ItemStack.EMPTY;
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
}
