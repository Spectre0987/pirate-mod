package net.pirates.mod.containers;

import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.network.IContainerFactory;
import net.pirates.mod.Pirates;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PContainers {

	public static ContainerType<BagContainer> BAG;
	public static ContainerType<CraftingContainer> CRAFTING;
	public static ContainerType<VanityContainer> VANITY;
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<ContainerType<?>> event) {
		event.getRegistry().registerAll(
			BAG = register(BagContainer::new, "bag"),
			CRAFTING = register(CraftingContainer::new, "crafting"),
			VANITY = register(VanityContainer::new, "vanity")
		);
	}
	
	public static <T extends Container> ContainerType<T> register(IContainerFactory<T> fact, String name){
		ContainerType<T> type = new ContainerType<T>(fact);
		type.setRegistryName(new ResourceLocation(Pirates.MODID, name));
		return type;
	}
}
