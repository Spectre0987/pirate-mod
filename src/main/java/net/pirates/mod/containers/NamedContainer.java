package net.pirates.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.pirates.mod.Pirates;

public class NamedContainer<T extends Container> implements INamedContainerProvider {

	TranslationTextComponent trans;
	ICont<T> cont;
	
	public NamedContainer(String name, ICont<T> cont) {
		this.cont = cont;
		this.trans = new TranslationTextComponent("container." + Pirates.MODID + "." + name);
	}
	
	@Override
	public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
		return cont.open(id, inv, player);
	}

	@Override
	public ITextComponent getDisplayName() {
		return this.trans;
	}
	
	@FunctionalInterface
	public static interface ICont<T extends Container>{
		T open(int id, PlayerInventory inv, PlayerEntity player);
	}

}
