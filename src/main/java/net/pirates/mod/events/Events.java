package net.pirates.mod.events;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.pirates.mod.Pirates;
import net.pirates.mod.cap.BagCapability;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.cap.Capabilities.GunProvider;
import net.pirates.mod.cap.GunCapability;
import net.pirates.mod.cap.IPlayerData;
import net.pirates.mod.cap.PlayerDataCapability;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.items.PItems;
import net.pirates.mod.net.Network;
import net.pirates.mod.net.packets.ServerJoinPacket;
import net.pirates.mod.recipes.Recipes;

@Mod.EventBusSubscriber(modid = Pirates.MODID)
public class Events {
	
	private static final ResourceLocation GUN_KEY = new ResourceLocation(Pirates.MODID, "gun");
	private static final ResourceLocation BAG_KEY = new ResourceLocation(Pirates.MODID, "bag");
	private static final ResourceLocation PLAYER_KEY = Helper.createRL("player");
	
	@SubscribeEvent
	public static void attachCapabilities(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() == PItems.flintlock) {
			event.addCapability(GUN_KEY, new GunProvider(new GunCapability()));
		}
		else if(event.getObject().getItem() == PItems.coin_purse) {
			event.addCapability(BAG_KEY, new Capabilities.BagProvider(new BagCapability(9, item -> item.getItem() == PItems.coin)));
		}
	}
	
	@SubscribeEvent
	public static void attachEntityCapabilities(AttachCapabilitiesEvent<Entity> event) {
		if(event.getObject() instanceof PlayerEntity)
			event.addCapability(PLAYER_KEY, new IPlayerData.Provider(new PlayerDataCapability((PlayerEntity)event.getObject())));
	}
	
	@SubscribeEvent
	public static void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
		if(event.getPlayer() instanceof ServerPlayerEntity)
			Network.sendToPlayer((ServerPlayerEntity)event.getPlayer(), new ServerJoinPacket(Recipes.RECIPES));
	}

}
