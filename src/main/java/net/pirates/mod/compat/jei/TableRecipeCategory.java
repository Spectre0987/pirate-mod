package net.pirates.mod.compat.jei;

import java.util.List;

import com.google.common.collect.Lists;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.blocks.PBlocks;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.recipes.TableRecipe;

public class TableRecipeCategory implements IRecipeCategory<TableRecipe> {

	public static final ResourceLocation KEY = Helper.createRL("table");
	
	public IDrawable background;
	public IDrawable icon;
	
	public TableRecipeCategory(IGuiHelper helper) {
		background = helper.createDrawable(Helper.createRL("textures/gui/compat/jei_table.png"), 0, 0, 89, 59);
		this.icon = helper.createDrawableIngredient(new ItemStack(PBlocks.CRAFTING_TABLE));
	}
	
	@Override
	public ResourceLocation getUid() {
		return KEY;
	}

	@Override
	public Class<? extends TableRecipe> getRecipeClass() {
		return TableRecipe.class;
	}

	@Override
	public String getTitle() {
		return "Pirates Table";
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(TableRecipe recipe, IIngredients ingredients) {
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, recipe.getResult());
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, TableRecipe recipe, IIngredients ingredients) {
		List<Ingredient> ingreds = recipe.getIngredients();
		
		int startX = 9, startY = 11;
		
		for(int i = 0; i < recipe.getIngredients().size(); ++i) {
			recipeLayout.getItemStacks().init(i, true, startX + (i / 2) * 18, startY + (i % 2) * 18);
			recipeLayout.getItemStacks().set(i, Lists.newArrayList(ingreds.get(i).getMatchingStacks()));
		}
		
		recipeLayout.getItemStacks().init(ingreds.size(), false, 60, 21);
		recipeLayout.getItemStacks().set(ingreds.size(), recipe.getResult());
		
	}

	
}
