package net.pirates.mod.compat;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.compat.jei.TableRecipeCategory;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.recipes.Recipes;

@JeiPlugin
public class JEICompat implements IModPlugin {

	public static final ResourceLocation KEY = Helper.createRL("jei");
	
	@Override
	public ResourceLocation getPluginUid() {
		return KEY;
	}

	@Override
	public void registerCategories(IRecipeCategoryRegistration reg) {
		reg.addRecipeCategories(new TableRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
	}

	@Override
	public void registerRecipes(IRecipeRegistration reg) {
		reg.addRecipes(Recipes.RECIPES, TableRecipeCategory.KEY);
	}

}
