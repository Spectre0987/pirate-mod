package net.pirates.mod.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;

public class ClientProxy implements IProxy {

	@Override
	public void openGui(int ID, GuiContext context) {
		
	}
	
	protected void open(Screen screen) {
		Minecraft.getInstance().displayGuiScreen(screen);
	}

}
