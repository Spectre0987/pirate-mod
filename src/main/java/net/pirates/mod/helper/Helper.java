package net.pirates.mod.helper;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.pirates.mod.Pirates;

public class Helper {

	public static int getAngleForFacing(Direction dir) {
		
		if(dir == null)
			return 0;
		
		switch(dir) {
		case NORTH: return 0;
		case EAST: return 90;
		case SOUTH: return 180;
		case WEST: return 270;
		default: return 0;
		}
	}

	public static List<Slot> fillPlayerSlots(PlayerInventory inv, int y) {
		List<Slot> slots = new ArrayList<>();
		
		for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            slots.add(new Slot(inv, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + y));
	         }
	      }

	      for(int i1 = 0; i1 < 9; ++i1) {
	         slots.add(new Slot(inv, i1, 8 + i1 * 18, 161 + y));
	      }
	      
	      return slots;

	}

	public static ResourceLocation createRL(String string) {
		return new ResourceLocation(Pirates.MODID, string);
	}
	
	public static void dropItemAtEntity(Entity ent, ItemStack stack) {
		ent.world.addEntity(new ItemEntity(ent.world, ent.posX, ent.posY, ent.posZ, stack));
	}

	public static Direction getFacingOr(BlockState state, Direction or) {
		if(state.has(BlockStateProperties.HORIZONTAL_FACING))
			return state.get(BlockStateProperties.HORIZONTAL_FACING);
		else if(state.has(BlockStateProperties.FACING_EXCEPT_UP))
			return state.get(BlockStateProperties.FACING_EXCEPT_UP);
		else if(state.has(BlockStateProperties.FACING))
			return state.get(BlockStateProperties.FACING);
		return or;
	}

}
