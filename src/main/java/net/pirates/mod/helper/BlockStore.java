package net.pirates.mod.helper;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.INBTSerializable;

public class BlockStore implements INBTSerializable<CompoundNBT>{

	private BlockState state = null;
	private CompoundNBT tile = null;
	
	public BlockStore(BlockState state, CompoundNBT tile) {
		this.state = state;
		this.tile = tile;
	}
	
	public BlockStore(BlockState state, TileEntity te) {
		this(state, te != null ? te.serializeNBT() : null);
	}
	
	public BlockStore(CompoundNBT tag) {
		this.deserializeNBT(tag);
	}
	
	public BlockStore(PacketBuffer buf) {
		BlockState state = NBTUtil.readBlockState(buf.readCompoundTag());
		boolean hasTile = buf.readBoolean();
		if(hasTile)
			this.tile = buf.readCompoundTag();
		
		this.state = state;
	}
	
	public BlockState getState() {
		return this.state;
	}
	
	public CompoundNBT getTileData() {
		return this.tile;
	}
	
	//Mostly for rendering
	public TileEntity createTile() {
		return tile == null ? null : TileEntity.create(tile);
	}
	
	public void encode(PacketBuffer buf) {
		buf.writeCompoundTag(NBTUtil.writeBlockState(state));
		buf.writeBoolean(this.tile != null);
		if(tile != null)
			buf.writeCompoundTag(this.tile);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("state", NBTUtil.writeBlockState(state));
		if(this.tile != null)
			tag.put("tile", this.tile);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.state = NBTUtil.readBlockState(tag.getCompound("state"));
		if(tag.contains("tile"))
			this.tile = tag.getCompound("tile");
	}
}
