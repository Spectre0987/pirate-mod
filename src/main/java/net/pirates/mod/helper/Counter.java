package net.pirates.mod.helper;

public class Counter {

	private int count = 0;
	
	public void add() {
		++this.count;
	}
	
	public int getCount() {
		return this.count;
	}
}
