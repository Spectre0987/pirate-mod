package net.pirates.mod.helper;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ShipHelper {

	
	public static HashMap<BlockPos, BlockStore> gatherShip(World world, BlockPos pos) {
		HashMap<BlockPos, BlockStore> blocks = Maps.newHashMap();
		
		Counter count = new Counter();
		getConnectedBlocks(world, pos, null, count, blocks);
		
		return blocks;
	}
	
	public static AxisAlignedBB getBounds(Map<BlockPos, BlockStore> store) {
		
		if(store.isEmpty())
			return null;
		
		int minX = 0, maxX = 0, minY = 0, maxY = 0, minZ = 0, maxZ = 0;
		
		for(BlockPos pos : store.keySet()) {
			
			if(pos.getX() < minX)
				minX = pos.getX();
			if(pos.getX() > maxX)
				maxX = pos.getX();
			
			if(pos.getY() < minY)
				minY = pos.getY();
			if(pos.getY() > maxY)
				maxY = pos.getY();
			
			if(pos.getZ() < minZ)
				minZ = pos.getZ();
			if(pos.getZ() > maxZ)
				maxZ = pos.getZ();
			
		}
		
		return new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
		
	}
	
	private static void getConnectedBlocks(World world, BlockPos pos, Direction comingFrom, Counter count, HashMap<BlockPos, BlockStore> map) {
		BlockState state = world.getBlockState(pos);
		
		//Stop if air or water
		if(state.isAir() || !state.getFluidState().isEmpty())
			return;
		
		//Stop if this block is already added (No back-tracking)
		if(map.containsKey(pos))
			return;
		
		//Stop if too many blocks
		if(count.getCount() > 1000)
			return;
		
		//Add to the list and increment the safety timer
		map.put(pos, new BlockStore(state, world.getTileEntity(pos)));
		count.add();
		
		//Get all connected
		for(Direction dir : Direction.values()) {
			if(dir != comingFrom)
				getConnectedBlocks(world, pos.offset(dir), dir.getOpposite(), count, map);
		}
	}
	
	public static BlockPos rotatePos(BlockPos pos, Rotation rot) {
		switch(rot) {
        default:
            return pos;
        case CLOCKWISE_90:
            return new BlockPos(-pos.getZ(), pos.getY(), pos.getX());
        case CLOCKWISE_180:
            return new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
        case COUNTERCLOCKWISE_90:
            return new BlockPos(pos.getZ(), pos.getY(), -pos.getX());
		}
	}
	
	public static BlockPos rotatePosAround(BlockPos origin, BlockPos pos, Rotation rot) {
		BlockPos newPos = rotatePos(pos.subtract(origin), rot);
		return newPos.add(origin);
	}
	
	public static Rotation getRotationFromShip(Direction current, Direction assembled) {
		int angle = Helper.getAngleForFacing(current) - Helper.getAngleForFacing(assembled);
		
		if(angle < 0)
			angle = 360 + angle;
		
		switch(angle) {
			default: return Rotation.NONE;
			case 90: return Rotation.CLOCKWISE_90;
			case 180: return Rotation.CLOCKWISE_180;
			case 270: return Rotation.COUNTERCLOCKWISE_90;
		}
	}
	
}
