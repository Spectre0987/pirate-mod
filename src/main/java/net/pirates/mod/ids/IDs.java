package net.pirates.mod.ids;

public class IDs {

	public static class Gui{
		
		private static int ID = 0;
		
		public static class Pirates{
			public static final int BLACKSMITH = Gui.getNextID();
		}
		
		public static int getNextID() {
			return ID + 1;
		}
	}
}
