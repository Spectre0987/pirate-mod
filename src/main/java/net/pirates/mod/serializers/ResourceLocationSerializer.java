package net.pirates.mod.serializers;

import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.IDataSerializer;
import net.minecraft.util.ResourceLocation;

public class ResourceLocationSerializer implements IDataSerializer<ResourceLocation> {

	@Override
	public ResourceLocation copyValue(ResourceLocation location) {
		return new ResourceLocation(location.toString());
	}

	@Override
	public ResourceLocation read(PacketBuffer buf) {
		return buf.readResourceLocation();
	}

	@Override
	public void write(PacketBuffer buf, ResourceLocation location) {
		buf.writeResourceLocation(location);
	}

}
