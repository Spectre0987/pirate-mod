package net.pirates.mod.serializers;

import net.minecraft.network.datasync.IDataSerializer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.DataSerializerEntry;
import net.pirates.mod.Pirates;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PDataSerializers {

	public static IDataSerializer<ResourceLocation> RESOURCE_LOCATION = new ResourceLocationSerializer();
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<DataSerializerEntry> event) {
		event.getRegistry().register(
				new DataSerializerEntry(RESOURCE_LOCATION).setRegistryName(new ResourceLocation(Pirates.MODID, "resource_location"))
		);
	}
}
