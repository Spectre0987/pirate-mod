package net.pirates.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.pirates.mod.Pirates;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.cap.IBagCap;
import net.pirates.mod.containers.BagContainer;
import net.pirates.mod.items.prop.Prop;

public class CoinPurseItem extends Item {

	public CoinPurseItem() {
		super(Prop.Items.ONE);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(!worldIn.isRemote && playerIn instanceof ServerPlayerEntity) {
					
			ItemStack stack = playerIn.getHeldItem(handIn);
			IBagCap bag = stack.getCapability(Capabilities.BAG).orElse(null);
			
			if(bag == null)
				return super.onItemRightClick(worldIn, playerIn, handIn);
			
			NetworkHooks.openGui((ServerPlayerEntity)playerIn,  new INamedContainerProvider() {

				@Override
				public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
					return new BagContainer(id, inv, stack);
				}

				@Override
				public ITextComponent getDisplayName() {
					return new TranslationTextComponent("inventory." + Pirates.MODID + ".coin_purse");
				}}, buf -> buf.writeItemStack(stack));
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

}
