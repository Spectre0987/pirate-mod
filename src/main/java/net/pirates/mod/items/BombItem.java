package net.pirates.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.pirates.mod.entities.BombEntity;
import net.pirates.mod.entities.PEntities;
import net.pirates.mod.items.prop.Prop;

public class BombItem extends Item {

	public BombItem() {
		super(Prop.Items.BASE_16);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(!worldIn.isRemote) {
			BombEntity bomb = PEntities.BOMB.create(worldIn);
			bomb.setPosition(playerIn.posX, playerIn.posY + playerIn.getEyeHeight(), playerIn.posZ);
			bomb.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0, 2, 0);
			worldIn.addEntity(bomb);
			if(!playerIn.isCreative())
				playerIn.getHeldItem(handIn).shrink(1);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}
