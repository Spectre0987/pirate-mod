package net.pirates.mod.items;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemTier;
import net.minecraft.item.SwordItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.pirates.mod.Pirates;
import net.pirates.mod.cap.IPlayerData.PirateEquipSlot;
import net.pirates.mod.foods.PFood;
import net.pirates.mod.itemgroups.Groups;
import net.pirates.mod.items.prop.Prop;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PItems {

	private static List<Item> ITEMS = new ArrayList<Item>();
	
	public static TelescopeItem telescope = register(new TelescopeItem(), "telescope");
	public static BaseItem hardtack = register(new BaseItem(new Item.Properties().food(PFood.HARDTACK).group(Groups.PIRATE)), "hardtack");
	public static BaseItem slowmatch = register(new BaseItem(Prop.Items.ONE), "slowmatch");
	public static BombItem bomb = register(new BombItem(), "bomb");
	public static ManifestItem manifest = register(new ManifestItem(), "manifest");
	public static LockpickItem lockpick = register(new LockpickItem(), "lockpick");
	public static BaseItem coin = register(new BaseItem(Prop.Items.BASE), "coin");
	public static CoinPurseItem coin_purse = register(new CoinPurseItem(), "coin_purse");
	public static BaseItem cartridge = register(new BaseItem(Prop.Items.BASE_16), "cartridge");
	public static GrappleHookItem GRAPPLE_HOOK = register(new GrappleHookItem(Prop.Items.ONE), "grapple_hook");
	public static BaseItem CANNON_BALL = register(new BaseItem(Prop.Items.BASE_16), "cannon_ball");
	
	//Clothes
	public static ClothesItem TRICORN = register(new ClothesItem(Prop.Items.ONE, EquipmentSlotType.HEAD), "tricorn");
	public static AccessoriesItem PEG_LEG = register(new AccessoriesItem(PirateEquipSlot.LEG, Prop.Items.ONE), "peg_leg");
	
	public static FlintlockItem flintlock = register(new FlintlockItem(), "flintlock");
	public static SwordItem rapier = register(new SwordItem(ItemTier.IRON, 2, 0F, Prop.Items.ONE), "rapier");
	public static SwordItem CUTLASS = register(new SwordItem(ItemTier.IRON, 3, -2.4F, Prop.Items.ONE), "cutlass");
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<Item> event) {
		for(Item item : ITEMS) {
			event.getRegistry().register(item);
		}
	}
	
	public static <T extends Item> T register(T item, String name) {
		item.setRegistryName(new ResourceLocation(Pirates.MODID, name));
		ITEMS.add(item);
		return item;
	}
	
	public static void addItem(Item item) {
		ITEMS.add(item);
	}
}
