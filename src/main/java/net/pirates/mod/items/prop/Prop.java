package net.pirates.mod.items.prop;

import net.minecraft.block.Block;
import net.minecraft.block.Block.Properties;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.common.ToolType;
import net.pirates.mod.itemgroups.Groups;

public class Prop {

	public static class Items{
		
		public static Item.Properties BASE = new Item.Properties().group(Groups.PIRATE);
		public static Item.Properties ONE = new Item.Properties().maxStackSize(1).group(Groups.PIRATE);
		public static Item.Properties BASE_16 = new Item.Properties().maxStackSize(16).group(Groups.PIRATE);
	}
	
	public static class Blocks{
		
		public static final Properties WOOD = Properties.create(Material.WOOD)
				.hardnessAndResistance(5F)
				.harvestTool(ToolType.AXE)
				.sound(SoundType.WOOD);
		
		public static final Properties IRON = Properties.create(Material.IRON)
				.hardnessAndResistance(5F)
				.harvestTool(ToolType.PICKAXE)
				.sound(SoundType.METAL);
		
		public static final Properties WOOL = Block.Properties.create(Material.WOOL)
				.hardnessAndResistance(0.8F)
				.sound(SoundType.CLOTH);
	}
}
