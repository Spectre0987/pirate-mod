package net.pirates.mod.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.pirates.mod.items.prop.Prop;

public class ManifestItem extends Item {

	public ManifestItem() {
		super(Prop.Items.ONE);
	}
	
	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		if(!context.getWorld().isRemote) {
			if(context.getItem().hasTag())
				context.getItem().getTag().putLong("crate_pos", context.getPos().toLong());
			else {
				CompoundNBT tag = new CompoundNBT();
				tag.putLong("crate_pos", context.getPos().toLong());
				context.getItem().setTag(tag);
			}
		}
		return ActionResultType.SUCCESS;
	}

}
