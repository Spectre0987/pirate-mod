package net.pirates.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.pirates.mod.Pirates;
import net.pirates.mod.cap.Capabilities;
import net.pirates.mod.cap.IGun;
import net.pirates.mod.cap.IGun.EnumGunState;
import net.pirates.mod.itemgroups.Groups;

public class FlintlockItem extends Item {

	public FlintlockItem() {
		super(new Item.Properties()
				.maxStackSize(1)
				.group(Groups.PIRATE)
				.maxDamage(250));
		this.addPropertyOverride(new ResourceLocation(Pirates.MODID, "state"), new IItemPropertyGetter() {

			@Override
			public float call(ItemStack stack, World world, LivingEntity entity) {
				IGun gun = stack.getCapability(Capabilities.GUN).orElse(null);
				if(gun != null) {
					return gun.getLoadedState() == EnumGunState.LOADED ? 1 : 0;
				}
				return 0;
			}});
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		playerIn.setActiveHand(handIn);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.BOW;
	}

	@Override
	public int getUseDuration(ItemStack stack) {
		return 72000;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity living, int timeLeft) {
		IGun gun = stack.getCapability(Capabilities.GUN).orElse(null);
		if(gun != null) {
			if(living instanceof PlayerEntity && gun.getCartridges() < 1 && this.getUseDuration(stack) - timeLeft > 60) {
				for(ItemStack inv : ((PlayerEntity)living).inventory.mainInventory) {
					if(inv.getItem() == PItems.cartridge) {
						inv.shrink(1);
						gun.setCartridges(1);
						if(!worldIn.isRemote)
							worldIn.playSound(null, living.getPosition(), SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.PLAYERS, 1F, 1.5F);
						return;
					}
				}
			}
			gun.shoot(worldIn, stack, living);
			
			if(gun.getCartridges() <= 0)
				gun.setLoadedState(EnumGunState.NOT_LOADED);
		}
		super.onPlayerStoppedUsing(stack, worldIn, living, timeLeft);
	}

	@Override
	public void onUsingTick(ItemStack stack, LivingEntity player, int count) {
		super.onUsingTick(stack, player, count);
		IGun gun = stack.getCapability(Capabilities.GUN).orElse(null);
		if(gun != null && player.isServerWorld()) {
			if(gun.getCartridges() < 1 && this.getUseDuration(stack) - count == 60) {
				player.world.playSound(null, player.getPosition(), SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.PLAYERS, 1F, 1.5F);
				gun.setLoadedState(EnumGunState.LOADED);
			}
		}
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		// TODO Auto-generated method stub
		return super.getDurabilityForDisplay(stack);
	}

	@Override
	public boolean getIsRepairable(ItemStack item, ItemStack repair) {
		return repair.getItem() == Items.IRON_INGOT;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip.pirates.gun").setStyle(new Style().setColor(TextFormatting.DARK_GRAY)));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

}
