package net.pirates.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.pirates.mod.config.PiratesConfig;
import net.pirates.mod.entities.GrappleHookEntity;
import net.pirates.mod.entities.PEntities;

public class GrappleHookItem extends Item {

	public GrappleHookItem(Properties properties) {
		super(properties);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(!worldIn.isRemote) {
			GrappleHookEntity hook = new GrappleHookEntity(PEntities.GRAPPLE, playerIn, worldIn);
			worldIn.addEntity(hook);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

}
