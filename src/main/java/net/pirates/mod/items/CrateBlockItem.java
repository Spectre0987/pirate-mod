package net.pirates.mod.items;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.pirates.mod.misc.CrateData;
import net.pirates.mod.tileentites.CrateTile;

public class CrateBlockItem extends BlockItem{

	public CrateBlockItem(Block blockIn, Properties builder) {
		super(blockIn, builder);
	}
	
	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
		if(entityIn instanceof LivingEntity) {
			LivingEntity liv = ((LivingEntity)entityIn);
			liv.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 20, 3));
		}
		if(getCrateData(stack) == null) {
			//setCrateData(stack, new CrateData("spices", 104, false));
			//setCrateData(stack, new CrateData("coffee", 52, false));
		}
	} 

	@Override
	public boolean onBlockPlaced(BlockPos pos, World worldIn, PlayerEntity player, ItemStack stack, BlockState state) {
		boolean placed = super.onBlockPlaced(pos, worldIn, player, stack, state);
		TileEntity te = worldIn.getTileEntity(pos);
		if(te instanceof CrateTile) {
			//Move items from the item to the Tile Entity
			CrateTile crate = (CrateTile)te;
			crate.setCrateData(getCrateData(stack));
		}
		return placed;
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		CrateData data = getCrateData(stack);
		if(data != null) {
			tooltip.add(data.trans);
			tooltip.add(new TranslationTextComponent("crate.pirates.data.price", data.basePrice));
		}
	}
	
	@Override
	public ITextComponent getDisplayName(ItemStack stack) {
		CrateData data = getCrateData(stack);
		if(data != null)
			return data.trans;
		
		return super.getDisplayName(stack);
	}
	
	public static void setCrateData(ItemStack stack, CrateData data) {
		if(data != null)
			stack.getOrCreateTag().put("crate", data.serializeNBT());
	}
	
	public static CrateData getCrateData(ItemStack stack) {
		
		if(stack.hasTag() && stack.getTag().contains("crate"))
			return new CrateData(stack.getTag().getCompound("crate"));
		return null;
	}
}
