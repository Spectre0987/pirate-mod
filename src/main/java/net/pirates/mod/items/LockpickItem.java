package net.pirates.mod.items;

import net.minecraft.item.Item;
import net.pirates.mod.items.prop.Prop;

public class LockpickItem extends Item{

	public LockpickItem() {
		super(Prop.Items.ONE.maxDamage(25));
	}

}
