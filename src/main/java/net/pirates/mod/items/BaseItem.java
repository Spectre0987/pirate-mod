package net.pirates.mod.items;

import net.minecraft.item.Item;
import net.pirates.mod.items.prop.Prop;

public class BaseItem extends Item {

	public BaseItem() {
		super(Prop.Items.BASE);
	}
	
	public BaseItem(Item.Properties prop) {
		super(prop);
	}

}
