package net.pirates.mod.items;

import net.minecraft.item.Item;
import net.pirates.mod.cap.IPlayerData.PirateEquipSlot;

public class AccessoriesItem extends Item{

	private PirateEquipSlot slot;
	private Runnable onEquip = null;
	private Runnable onUnequip = null;
	
	public AccessoriesItem(PirateEquipSlot slot, Properties properties) {
		super(properties);
		this.slot = slot;
	}
	
	public PirateEquipSlot getSlot() {
		return this.slot;
	}
	
	public Runnable getEquipAction() {
		return this.onEquip;
	}
	
	public Runnable getUnequipAction() {
		return this.onUnequip;
	}
	
	public void setEquipAction(Runnable run) {
		this.onEquip = run;
	}

	public void setUnequipAction(Runnable run) {
		this.onUnequip = run;
	}
	
}
