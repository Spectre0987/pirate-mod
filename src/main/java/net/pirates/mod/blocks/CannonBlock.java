package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.Tags;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;
import net.pirates.mod.entities.CannonBallEntity;
import net.pirates.mod.entities.PEntities;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.items.PItems;
import net.pirates.mod.items.prop.Prop;

public class CannonBlock extends Block {

	public static final IntegerProperty LOAD_STATE = IntegerProperty.create("load", 0, 2);
	private static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(createBaseShape());
	
	public CannonBlock() {
		super(Prop.Blocks.IRON);
		this.setDefaultState(this.stateContainer.getBaseState().with(LOAD_STATE, 0).with(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH));
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(LOAD_STATE, BlockStateProperties.HORIZONTAL_FACING);
	}
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		
		ItemStack stack = player.getHeldItem(handIn);
		
		CannonState cannon = getCannonState(state);
		
		//Gun powder
		if(cannon == CannonState.EMPTY && stack.getItem().isIn(Tags.Items.GUNPOWDER)) {
			worldIn.setBlockState(pos, setCannonState(state, CannonState.POWDER));
			stack.shrink(1);
			return true;
		}
		
		//Ball
		if(cannon == CannonState.POWDER && stack.getItem() == PItems.CANNON_BALL) {
			worldIn.setBlockState(pos, setCannonState(state, CannonState.BALL));
			stack.shrink(1);
			return true;
		}
		
		//FIRE!!!!
		if(cannon == CannonState.BALL && (stack.getItem() == PItems.slowmatch || stack.getItem() == Items.FLINT_AND_STEEL)) {
			worldIn.setBlockState(pos, setCannonState(state, CannonState.EMPTY));
			if(player instanceof ServerPlayerEntity)
				stack.attemptDamageItem(1, player.getRNG(), (ServerPlayerEntity)player);
			
			Direction face = Helper.getFacingOr(state, Direction.NORTH);
			BlockPos ballPos = pos.toImmutable().offset(face);
			
			//Fire cannon ball
			if(!worldIn.isRemote) {
				CannonBallEntity ball = PEntities.CANNON_BALL.create(worldIn);
				
				ball.setPositionAndUpdate(ballPos.getX() + 0.5, ballPos.getY() + 0.5, ballPos.getZ() + 0.5);
				
				BlockPos target = BlockPos.ZERO.offset(face);
				ball.shoot(target.getX(), target.getY(), target.getZ(), 3, 0);
				
				worldIn.addEntity(ball);
				worldIn.playSound(null, pos, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1F, 1F);
			}
			else {
				//Particles
				
				float angle = (float)Math.toRadians(Helper.getAngleForFacing(face));
				float speed = 0.1F;
				
				for(int i = 0; i < 20; ++i) {
					
					worldIn.addParticle(worldIn.rand.nextBoolean() ? ParticleTypes.CLOUD : ParticleTypes.SMOKE, ballPos.getX() + 0.5, ballPos.getY() + 0.5, ballPos.getZ() + 0.5, Math.sin(angle) * speed, 0, -Math.cos(angle) * speed);
				}
				
			}
			return true;
		}
		
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return SHAPE.getShapeFor(Helper.getFacingOr(state, Direction.NORTH));
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return super.getStateForPlacement(context).with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing());
	}

	public static BlockState setCannonState(BlockState state, CannonState cannon) {
		return state.with(LOAD_STATE, cannon.ordinal());
	}
	
	public static CannonState getCannonState(BlockState state) {
		return !state.has(LOAD_STATE) ? CannonState.EMPTY : CannonState.getByID(state.get(LOAD_STATE));
	}

	public static VoxelShape createBaseShape() {
		return Stream.of(
				Block.makeCuboidShape(14, 1, 14, 15, 2, 15),
				Block.makeCuboidShape(1.5, 0, 0, 2.5, 3, 3),
				Block.makeCuboidShape(1.5, 0, 13, 2.5, 3, 16),
				Block.makeCuboidShape(13.5, 0, 13, 14.5, 3, 16),
				Block.makeCuboidShape(13.5, 0, 0, 14.5, 3, 3),
				Block.makeCuboidShape(2.5, 1, 1, 13.5, 4, 15),
				Block.makeCuboidShape(2.5, 4, 1, 4.5, 7, 11),
				Block.makeCuboidShape(2.5, 7, 1, 4.5, 10, 7),
				Block.makeCuboidShape(11.5, 7, 1, 13.5, 10, 7),
				Block.makeCuboidShape(11.5, 4, 1, 13.5, 7, 11),
				Block.makeCuboidShape(5, 5.5, -2, 11, 11.5, 10),
				Block.makeCuboidShape(4.5, 5, 10, 11.5, 12, 16),
				Block.makeCuboidShape(4.5, 5, -3, 11.5, 12, -2),
				Block.makeCuboidShape(2, 7.5, 5, 14, 8.5, 6),
				Block.makeCuboidShape(1, 1, 1, 2, 2, 2),
				Block.makeCuboidShape(1, 1, 14, 2, 2, 15),
				Block.makeCuboidShape(14, 1, 1, 15, 2, 2)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}
	
	public static enum CannonState{
		EMPTY,
		POWDER,
		BALL;
		
		static CannonState getByID(int id) {
			if(id >= CannonState.values().length)
				return CannonState.EMPTY;
			return CannonState.values()[id];
		}
	}
}
