package net.pirates.mod.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.pirates.mod.Pirates;
import net.pirates.mod.items.PItems;
import net.pirates.mod.items.prop.Prop;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Bus.MOD)
public class PBlocks {
	
	public static List<Block> BLOCKS = new ArrayList<>();
	
	public static RailingBlock RAILINGS = register(new RailingBlock(), "railings");
	public static TransparentRailingBlock GHASTLY_RAILINGS = register(new TransparentRailingBlock(), "ghastly_railings");
	
	public static BowspritBlock BOWSPRIT = register(new BowspritBlock(), "bowsprit");
	public static HorizontalFacingBaseBlock BOWSPRIT_SPAR = register(new BowspritSparBlock(Prop.Blocks.WOOD), "bowsprit_spar");
	
	public static HatchBasicBlock HATCH_BASIC = register(new HatchBasicBlock(), "hatch_basic");
	public static HatchSolidBlock HATCH_SOLID = register(new HatchSolidBlock(), "hatches/hatch_solid");
	public static CrateBlock CRATE = register(new CrateBlock(), "crate");
	public static SlingBlock SLING = register(new SlingBlock(Prop.Blocks.WOOD), "boat_sling");
	public static CraftingTable CRAFTING_TABLE = register(new CraftingTable(Prop.Blocks.WOOD), "crafting_table");
	public static VanityBlock VANITY = register(new VanityBlock(Prop.Blocks.WOOD), "vanity");
	public static ChainBlock CHAIN = register(new ChainBlock(Prop.Blocks.IRON), "chain");
	public static StepsBlock STEPS = register(new StepsBlock(Prop.Blocks.WOOD), "steps");
	public static ShipWheelBlock SHIPS_WHEEL = register(new ShipWheelBlock(Prop.Blocks.WOOD), "ships_wheel");
	public static CrowsNestBlock CROWS_NEST = register(new CrowsNestBlock(Prop.Blocks.WOOD), "crows_nest");
	public static RatlinesBlock RATLINES = register(new RatlinesBlock(Prop.Blocks.WOOD), "ratlines");
	public static RopeLadderBlock ROPE_LADDER = register(new RopeLadderBlock(), "rope_ladder");
	public static CannonBlock CANNON = register(new CannonBlock(), "cannon");
	
	//Decoration Blocks
	public static TransparentCubeBlock GHASTLY_PLANKS = register(new TransparentCubeBlock(Prop.Blocks.WOOD), "wood/ghastly_planks");
	public static TransparentLogsBlock GHASTLY_LOGS = register(new TransparentLogsBlock(Prop.Blocks.WOOD), "wood/ghastly_logs");
	public static LampBlock LAMP = register(new LampBlock(Prop.Blocks.IRON), "lamp");
	
	//Sails
	public static MainSailBlock MAINSAIL = register(new MainSailBlock(), "main_sail");
	
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<Block> event) {
		for(Block block : BLOCKS) {
			event.getRegistry().register(block);
		}
		
		BLOCKS.clear();
	}
	
	public static <T extends Block> T register(T block, String name) {
		ResourceLocation loc = new ResourceLocation(Pirates.MODID, name);
		block.setRegistryName(loc);
		
		BlockItem item;
		
		if(block instanceof ISpecialItem)
			item = ((ISpecialItem)block).getItem();
		else {
			item = new BlockItem(block, Prop.Items.BASE);
		}
		item.setRegistryName(loc);
		PItems.addItem(item);
		BLOCKS.add(block);
		
		return block;
	}

}
