package net.pirates.mod.blocks;

import java.util.Random;
import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;

public class LampBlock extends Block {

	public static final IntegerProperty LIGHT = IntegerProperty.create("light", 0, 15);
	public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(createVoxelShape());
	
	public LampBlock(Properties properties) {
		super(properties);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return super.getStateForPlacement(context);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.HORIZONTAL_FACING, LIGHT);
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.TRANSLUCENT;
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation direction) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, direction.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

	@Override
	public int getLightValue(BlockState state) {
		return state.get(LIGHT);
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return SHAPE.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(handIn == Hand.MAIN_HAND) {
			if(!worldIn.isRemote) {
				int light = state.get(LIGHT);
				if(player.isSneaking() && light > 0) {
					worldIn.setBlockState(pos, state.with(LIGHT, light - 1));
				}
				else if(light < 15){
					worldIn.setBlockState(pos, state.with(LIGHT, light + 1));
				}
			}
			return false;
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

	@Override
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
		super.animateTick(stateIn, worldIn, pos, rand);
		if(stateIn.get(LIGHT) > 0)
			worldIn.addParticle(ParticleTypes.FLAME, pos.getX() + 0.5, pos.getY() + 0.6, pos.getZ() + 0.5, 0, 0, 0);
		if(stateIn.get(LIGHT) > 13)
			worldIn.addParticle(ParticleTypes.SMOKE, pos.getX() + 0.5, pos.getY() + 0.9, pos.getZ() + 0.5, 0, 0, 0);
	}
	
	public static VoxelShape createVoxelShape() {
		return Stream.of(
				Block.makeCuboidShape(5, 0, 5, 11, 3, 11),
				Block.makeCuboidShape(6.5, 3, 6.5, 9.5, 6, 9.5),
				Block.makeCuboidShape(5.25, 3.75, 7.6, 5.5, 4.75, 8.5),
				Block.makeCuboidShape(5.5, 4, 7.85, 6.5, 4.5, 8.25),
				Block.makeCuboidShape(5.5, 9, 5.5, 6.5, 17, 10.5),
				Block.makeCuboidShape(6.5, 9, 5.5, 9.5, 17, 6.5),
				Block.makeCuboidShape(9.5, 9, 5.5, 10.5, 17, 10.5),
				Block.makeCuboidShape(6.5, 9, 9.5, 9.5, 17, 10.5),
				Block.makeCuboidShape(5, 7, 5.5, 5.5, 9, 10.5),
				Block.makeCuboidShape(5, 7, 10.5, 11, 9, 11),
				Block.makeCuboidShape(5, 7, 5, 11, 9, 5.5),
				Block.makeCuboidShape(10.5, 7, 5.5, 11, 9, 10.5),
				Block.makeCuboidShape(5.5, 5, 5.5, 6.5, 7, 10.5),
				Block.makeCuboidShape(6.5, 5, 9.5, 9.5, 7, 10.5),
				Block.makeCuboidShape(9.5, 5, 5.5, 10.5, 7, 10.5),
				Block.makeCuboidShape(6.5, 5, 5.5, 9.5, 7, 6.5)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

}
