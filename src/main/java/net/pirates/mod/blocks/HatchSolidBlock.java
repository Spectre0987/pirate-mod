package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;

public class HatchSolidBlock extends HatchBasicBlock {

	@Override
	public VoxelShape createClosedShape() {
		return Stream.of(
				Block.makeCuboidShape(0, 0, 0, 16, 4, 1),
				Block.makeCuboidShape(0, 0, 15, 16, 4, 16),
				Block.makeCuboidShape(0, 0, 1, 1, 4, 15),
				Block.makeCuboidShape(15, 0, 1, 16, 4, 15),
				Block.makeCuboidShape(-1, 4, -1, 17, 5, 17)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public VoxelShape createOpenShape() {
		return Stream.of(
				Block.makeCuboidShape(0, 0, 0, 16, 4, 1),
				Block.makeCuboidShape(0, 0, 15, 16, 4, 16),
				Block.makeCuboidShape(0, 0, 1, 1, 4, 15),
				Block.makeCuboidShape(15, 0, 1, 16, 4, 15)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

}
