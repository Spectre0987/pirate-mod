package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;
import net.pirates.mod.containers.NamedContainer;
import net.pirates.mod.containers.VanityContainer;
import net.pirates.mod.helper.Helper;

public class VanityBlock extends Block implements IWaterLoggable{

	private DirectionalVoxelShape shape = new DirectionalVoxelShape(this.createShape());
	
	public VanityBlock(Properties properties) {
		super(properties);
		this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false));
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			NetworkHooks.openGui((ServerPlayerEntity)player, new NamedContainer<>("custom", VanityContainer::new));
		}
		return true;
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	public IFluidState getFluidState(BlockState state) {
		return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getDefaultState() : Fluids.EMPTY.getDefaultState();
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
	}
	
	public VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(2, 20, 13.75, 14, 31, 14),
				Block.makeCuboidShape(0, 0, 0, 16, 1, 16),
				Block.makeCuboidShape(1, 1, 1, 15, 15, 15),
				Block.makeCuboidShape(2.5, 9, 0, 13.5, 13, 1),
				Block.makeCuboidShape(2.5, 3, 0, 13.5, 7, 1),
				Block.makeCuboidShape(0, 15, 0, 16, 16, 16),
				Block.makeCuboidShape(0, 16, 13, 1, 25, 14),
				Block.makeCuboidShape(15, 16, 13, 16, 25, 14),
				Block.makeCuboidShape(14, 19, 13, 15, 32, 14),
				Block.makeCuboidShape(1, 19, 13, 2, 32, 14),
				Block.makeCuboidShape(2, 19, 13, 14, 20, 14),
				Block.makeCuboidShape(2, 31, 13, 14, 32, 14)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(Helper.getFacingOr(state, Direction.NORTH));
	}

	@Override
	public BlockState updatePostPlacement(BlockState state, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
		boolean water = worldIn.getFluidState(currentPos).isTagged(FluidTags.WATER);
		return state.with(BlockStateProperties.WATERLOGGED, water);
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

}
