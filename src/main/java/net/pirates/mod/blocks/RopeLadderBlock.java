package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;

public class RopeLadderBlock extends Block{

	public RopeLadderBlock() {
		super(Properties.create(Material.WOOL).hardnessAndResistance(3F));
	}
	
	public VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(0, 13.3, 13, 16, 14.3, 16),
				Block.makeCuboidShape(1, 0, 14, 2, 16, 15),
				Block.makeCuboidShape(14, 0, 14, 15, 16, 15),
				Block.makeCuboidShape(0, 2.6, 13, 16, 3.6, 16),
				Block.makeCuboidShape(0, 8, 13, 16, 9, 16)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

}
