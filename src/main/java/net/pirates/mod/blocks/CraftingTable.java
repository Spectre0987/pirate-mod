package net.pirates.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.pirates.mod.Pirates;
import net.pirates.mod.containers.CraftingContainer;

public class CraftingTable extends TileBlock {

	public CraftingTable(Properties properties) {
		super(properties);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			worldIn.getTileEntity(pos).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(table -> {
				NetworkHooks.openGui((ServerPlayerEntity)player, new INamedContainerProvider() {

					@Override
					public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
						return new CraftingContainer(id, inv, (IItemHandlerModifiable)table);
					}

					@Override
					public ITextComponent getDisplayName() {
						return new TranslationTextComponent("screen." + Pirates.MODID + ".crafting_table");
					}
				}, buf -> buf.writeBlockPos(pos));
			});
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}
}
