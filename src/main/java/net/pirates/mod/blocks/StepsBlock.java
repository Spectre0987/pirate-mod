package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.pirates.mod.blocks.misc.VoxelShapeHelper;

public class StepsBlock extends Block implements IWaterLoggable{

	private VoxelShape NORTH_SHAPE = createShape();
	private VoxelShape EAST_SHAPE = VoxelShapeHelper.rotateShape(Direction.EAST, NORTH_SHAPE);
	private VoxelShape SOUTH_SHAPE = VoxelShapeHelper.rotateShape(Direction.SOUTH, NORTH_SHAPE);
	private VoxelShape WEST_SHAPE = VoxelShapeHelper.rotateShape(Direction.WEST, NORTH_SHAPE);
	
	public StepsBlock(Properties properties) {
		super(properties);
		this.setDefaultState(this.stateContainer.getBaseState().with(BlockStateProperties.WATERLOGGED, false)
				.with(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH));
	}
	
	public VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(1, 2, 1, 15, 4, 6),
				Block.makeCuboidShape(1, 7, 6, 15, 9, 11),
				Block.makeCuboidShape(1, 13, 12, 15, 15, 17)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);
		switch(dir) {
			case EAST: return EAST_SHAPE;
			case SOUTH: return SOUTH_SHAPE;
			case WEST: return WEST_SHAPE;
			default: return NORTH_SHAPE;
		}
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState()
				.with(BlockStateProperties.WATERLOGGED, context.getWorld().getFluidState(context.getPos()).isTagged(FluidTags.WATER))
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.WATERLOGGED, BlockStateProperties.HORIZONTAL_FACING);
	}

	@Override
	public boolean isSolid(BlockState state) {
		return false;
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

}
