package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;

public class CrowsNestBlock extends HorizontalFacingBaseBlock{

	private DirectionalVoxelShape shape = new DirectionalVoxelShape(this.createShape());
	
	public CrowsNestBlock(Properties properties) {
		super(properties);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	public VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(0, 0, 0, 16, 1, 16),
				Block.makeCuboidShape(6.5, 1, 6.5, 9.5, 12, 9.5),
				Block.makeCuboidShape(7, 8, 9.5, 9, 27, 11.5),
				Block.makeCuboidShape(15, 1, 15, 16, 9, 16),
				Block.makeCuboidShape(15, 1, 12, 16, 9, 13),
				Block.makeCuboidShape(15, 1, 9, 16, 9, 10),
				Block.makeCuboidShape(15, 1, 3, 16, 9, 4),
				Block.makeCuboidShape(15, 1, 6, 16, 9, 7),
				Block.makeCuboidShape(0, 1, 0, 1, 9, 1),
				Block.makeCuboidShape(0, 1, 3, 1, 9, 4),
				Block.makeCuboidShape(0, 1, 6, 1, 9, 7),
				Block.makeCuboidShape(0, 1, 9, 1, 9, 10),
				Block.makeCuboidShape(0, 1, 12, 1, 9, 13),
				Block.makeCuboidShape(0, 1, 15, 1, 9, 16),
				Block.makeCuboidShape(3, 1, 0, 4, 9, 1),
				Block.makeCuboidShape(9, 1, 0, 10, 9, 1),
				Block.makeCuboidShape(6, 1, 0, 7, 9, 1),
				Block.makeCuboidShape(15, 1, 0, 16, 9, 1),
				Block.makeCuboidShape(12, 1, 0, 13, 9, 1),
				Block.makeCuboidShape(3, 1, 15, 4, 9, 16),
				Block.makeCuboidShape(6, 1, 15, 7, 9, 16),
				Block.makeCuboidShape(9, 1, 15, 10, 9, 16),
				Block.makeCuboidShape(12, 1, 15, 13, 9, 16),
				Block.makeCuboidShape(-1, 9, 15, 17, 10, 17),
				Block.makeCuboidShape(-1, 9, -1, 17, 10, 1),
				Block.makeCuboidShape(15, 9, 1, 17, 10, 15),
				Block.makeCuboidShape(-1, 9, 1, 1, 10, 15),
				Block.makeCuboidShape(6, 9, 6, 10, 10, 12)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

}
