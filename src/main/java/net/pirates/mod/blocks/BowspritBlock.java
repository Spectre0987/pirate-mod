package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;
import net.pirates.mod.items.prop.Prop;

public class BowspritBlock extends Block {
	
	public static final BooleanProperty FULL = BooleanProperty.create("full");
	public DirectionalVoxelShape empty_shape = new DirectionalVoxelShape(this.createEmptyShape());
	public DirectionalVoxelShape full_shape = new DirectionalVoxelShape(this.createFullShape());

	public BowspritBlock() {
		super(Prop.Blocks.WOOD);
		this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false)
				.with(FULL, false));
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		BlockState state = this.getDefaultState()
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
		return state;
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.WATERLOGGED);
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
		builder.add(FULL);
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public boolean isSolid(BlockState state) {
		return false;
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);
		return state.get(FULL) ? this.full_shape.getShapeFor(dir) : this.empty_shape.getShapeFor(dir);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			ItemStack stack = player.getHeldItem(handIn);
			if(state.get(FULL) && stack.isEmpty()) {
				
				//See if there is a spar in front of this
				Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);
				
				BlockPos otherPos = pos.offset(dir);
				while(worldIn.getBlockState(otherPos).getBlock() == PBlocks.BOWSPRIT_SPAR) {
					otherPos = otherPos.offset(dir);
				}
				
				otherPos = otherPos.offset(dir.getOpposite());
				
				if(!otherPos.equals(pos) && worldIn.getBlockState(otherPos).getBlock() == PBlocks.BOWSPRIT_SPAR) {
					worldIn.setBlockState(otherPos, Blocks.AIR.getDefaultState(), 3);
					InventoryHelper.spawnItemStack(worldIn, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, new ItemStack(PBlocks.BOWSPRIT_SPAR));
					return true;
				}
				
				
				worldIn.setBlockState(pos, state.with(FULL, false));
				InventoryHelper.spawnItemStack(worldIn, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, new ItemStack(PBlocks.BOWSPRIT_SPAR));
				return true;
			}
			else if(stack.getItem() == Item.getItemFromBlock(PBlocks.BOWSPRIT_SPAR)) {
				
				if(state.get(FULL)) {
					Direction dir  = state.get(BlockStateProperties.HORIZONTAL_FACING);
					
					BlockPos otherPos = pos.offset(dir);
					
					while(worldIn.getBlockState(otherPos).getBlock() == PBlocks.BOWSPRIT_SPAR) {
						otherPos = otherPos.offset(dir);
					}
					
					if(!worldIn.getBlockState(otherPos).isSolid()) {
						worldIn.setBlockState(otherPos, PBlocks.BOWSPRIT_SPAR.getDefaultState()
								.with(BlockStateProperties.HORIZONTAL_FACING, state.get(BlockStateProperties.HORIZONTAL_FACING))
								.with(BlockStateProperties.WATERLOGGED, worldIn.getFluidState(otherPos).isTagged(FluidTags.WATER)));
					}
					
					return true;
				}
				else {
					worldIn.setBlockState(pos, state.with(FULL, true));
					if(!player.isCreative())
						stack.shrink(1);
					return true;
				}
			}
		}
		return false;
	}
	
	public VoxelShape createEmptyShape() {
		return Stream.of(
				Block.makeCuboidShape(10, 0, 7, 11, 4.5, 8),
				Block.makeCuboidShape(4, 0, 7, 5, 4.5, 8),
				Block.makeCuboidShape(3.25, 4.5, 7, 11.75, 5.5, 8)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}
	
	public VoxelShape createFullShape() {
		return Stream.of(
				Block.makeCuboidShape(10, 0, 7, 11, 4.5, 8),
				Block.makeCuboidShape(4, 0, 7, 5, 4.5, 8),
				Block.makeCuboidShape(3.25, 4.5, 7, 11.75, 5.5, 8),
				Block.makeCuboidShape(5.5, 0, 0, 9.5, 4, 16)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}

}
