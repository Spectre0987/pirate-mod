package net.pirates.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;

public class BowspritSparBlock extends HorizontalFacingBaseBlock {

	private DirectionalVoxelShape shape = new DirectionalVoxelShape(Block.makeCuboidShape(5.5, 0, 0, 9.5, 4, 16));
	
	public BowspritSparBlock(Properties properties) {
		super(properties);
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

}
