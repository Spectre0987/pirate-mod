package net.pirates.mod.blocks.misc;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;

public class VoxelShapeHelper {

	public static VoxelShape rotateShape(Direction dir, VoxelShape shape) {
		
		ArrayList<VoxelShape> rotatedShapes = Lists.newArrayList();
		
		for(AxisAlignedBB bb : shape.toBoundingBoxList()) {
			rotatedShapes.add(VoxelShapes.create(rotateBox(bb.offset(-0.5, -0.5, -0.5), dir).offset(0.5, 0.5, 0.5)));
		}
		
		return combineAll(rotatedShapes);
		
	}
	
	public static AxisAlignedBB rotateBox(AxisAlignedBB box, Direction dir) {
		switch(dir) {
	        default:
	            return box;
	        case EAST:
	            return new AxisAlignedBB(-box.minZ, box.minY, box.minX, -box.maxZ, box.maxY, box.maxX);
	        case SOUTH:
	            return new AxisAlignedBB(-box.minX, box.minY, -box.minZ, -box.maxX, box.maxY, -box.maxZ);
	        case WEST:
	            return new AxisAlignedBB(box.minZ, box.minY, -box.minX, box.maxZ, box.maxY, -box.maxX);
		}
	}
	
	public static VoxelShape combineAll(List<VoxelShape> shapes) {
		VoxelShape first = shapes.get(0);
		if(shapes.size() > 1) {
			for(int i = 1; i < shapes.size(); ++i) {
				first = VoxelShapes.combine(first, shapes.get(i), IBooleanFunction.OR);
			}
		}
		return first;
	}
}
