package net.pirates.mod.blocks.misc;

import net.minecraft.util.Direction;
import net.minecraft.util.math.shapes.VoxelShape;

public class DirectionalVoxelShape {

	public VoxelShape base;
	public VoxelShape east;
	public VoxelShape south;
	public VoxelShape west;
	
	public DirectionalVoxelShape(VoxelShape shape) {
		this.base = shape;
		this.east = VoxelShapeHelper.rotateShape(Direction.EAST, shape);
		this.south = VoxelShapeHelper.rotateShape(Direction.SOUTH, shape);
		this.west = VoxelShapeHelper.rotateShape(Direction.WEST, shape);
	}
	
	public VoxelShape getShapeFor(Direction dir) {
		switch(dir){
			case EAST: return east;
			case SOUTH: return south;
			case WEST: return west;
			default: return base;
		}
	}
}
