package net.pirates.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.IBlockReader;

public class TileBlock extends Block{

	private TileEntityType<?> tileType;
	
	public TileBlock(Properties properties) {
		super(properties);
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return this.tileType.create();
	}
	
	public void setTileType(TileEntityType<?> type) {
		this.tileType = type;
	}
	
	public TileEntityType<?> getTileType() {
		return this.tileType;
	}

}
