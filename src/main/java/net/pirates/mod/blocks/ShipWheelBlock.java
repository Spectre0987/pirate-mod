package net.pirates.mod.blocks;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;
import net.pirates.mod.entities.ShipEntity;
import net.pirates.mod.helper.BlockStore;
import net.pirates.mod.helper.Helper;
import net.pirates.mod.helper.ShipHelper;
import net.pirates.mod.ship.ShipSize;
import net.pirates.mod.tileentites.PTiles;

public class ShipWheelBlock extends HorizontalFacingBaseBlock{

	private DirectionalVoxelShape shape = new DirectionalVoxelShape(this.createShape());
	
	public ShipWheelBlock(Properties properties) {
		super(properties);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

	public VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(0, 3, 2.5, 16, 19, 4.5),
				Block.makeCuboidShape(3, 0, 14, 13, 16, 15),
				Block.makeCuboidShape(4, 0, 6, 12, 15, 14),
				Block.makeCuboidShape(3, 0, 5, 13, 16, 6)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return PTiles.SHIP_WHEEL.get().create();
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		/*
		if(!worldIn.isRemote) {
			
			HashMap<BlockPos, BlockStore> map = ShipHelper.gatherShip(worldIn, pos);
			
			int flag = 2;
			
			worldIn.getServer().enqueue(new TickDelayedTask(1, () -> {
				List<BlockPos> secondPass = Lists.newArrayList();
				for(BlockPos store : map.keySet()) {
					
					try {
						//Clear invs, and such
						TileEntity te = worldIn.getTileEntity(store);
						if(te != null)
							worldIn.getTileEntity(store).read(new CompoundNBT());
					}
					catch(Exception e) {}
					
					if(!map.get(store).getState().isSolid()) {
						worldIn.setBlockState(store, Blocks.AIR.getDefaultState(), flag);
					}
					else secondPass.add(store);
				}
				
				for(BlockPos p : secondPass) {
					worldIn.setBlockState(p, Blocks.AIR.getDefaultState(), flag);
				}
			}));
			
			float angle = Helper.getAngleForFacing(state.get(BlockStateProperties.HORIZONTAL_FACING));
			
			ShipSize shipSize = new ShipSize(map.keySet());
			
			int offsetY = Math.round(shipSize.getSize().height);
			
			ShipEntity ship = new ShipEntity(worldIn);
			ship.setFacing(state.get(BlockStateProperties.HORIZONTAL_FACING));
			ship.getShell().setOffset(pos.down((int)(shipSize.getSize().height / 3.0)));
			ship.getShell().putBlocks(map);
			ship.setShipSize(shipSize.getSize());
			ship.setPosition(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5);
			ship.prevRotationYaw = ship.rotationYaw = angle;
			worldIn.addEntity(ship);
			player.startRiding(ship);
			
		}
		*/
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

}
