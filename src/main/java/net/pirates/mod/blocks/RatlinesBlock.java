package net.pirates.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;

public class RatlinesBlock extends HorizontalFacingBaseBlock {

	public static final BooleanProperty BOTTOM = BooleanProperty.create("bottom");
	public DirectionalVoxelShape shape = new DirectionalVoxelShape(Block.makeCuboidShape(0, 0, 13, 16, 16, 16));
	
	public RatlinesBlock(Properties properties) {
		super(properties);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BOTTOM);
	}

	@Override
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
		boolean bottom = !(worldIn.getBlockState(currentPos.down()).getBlock() instanceof RatlinesBlock);
		return stateIn.with(BOTTOM, bottom);
	}

	@Override
	public boolean isLadder(BlockState state, IWorldReader world, BlockPos pos, LivingEntity entity) {
		return true;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

}
