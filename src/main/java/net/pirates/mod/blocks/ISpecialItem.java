package net.pirates.mod.blocks;

import net.minecraft.item.BlockItem;

public interface ISpecialItem {

	BlockItem getItem();
}
