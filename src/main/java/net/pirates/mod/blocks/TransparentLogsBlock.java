package net.pirates.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.LogBlock;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

public class TransparentLogsBlock extends LogBlock {


	public TransparentLogsBlock(Properties properties) {
		super(MaterialColor.BROWN, properties);
	}


	@Override
	public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return 0;
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.TRANSLUCENT;
	}
	
	@Override
	public boolean isSideInvisible(BlockState state, BlockState adjacentBlockState, Direction side) {
		
		if(adjacentBlockState.getBlock() == state.getBlock())
			return true;
		
		return super.isSideInvisible(state, adjacentBlockState, side);
	}
	
}
