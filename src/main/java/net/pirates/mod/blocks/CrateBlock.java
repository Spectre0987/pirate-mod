package net.pirates.mod.blocks;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.BlockState;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootContext.Builder;
import net.pirates.mod.items.CrateBlockItem;
import net.pirates.mod.items.prop.Prop;
import net.pirates.mod.tileentites.CrateTile;

public class CrateBlock extends TileBlock implements ISpecialItem{

	public BlockItem item;
	
	public CrateBlock() {
		super(Prop.Blocks.WOOD);
		item = new CrateBlockItem(this, Prop.Items.BASE);
	}
	
	@Override
	public BlockItem getItem() {
		return item;
	}

	@Override
	public void dropXpOnBlockBreak(World worldIn, BlockPos pos, int amount) {}

	@Override
	public boolean canDropFromExplosion(Explosion explosionIn) {
		return true;
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, Builder builder) {
		return Lists.newArrayList();
	}

	@Override
	public void harvestBlock(World worldIn, PlayerEntity player, BlockPos pos, BlockState state, TileEntity te, ItemStack stack) {
		super.harvestBlock(worldIn, player, pos, state, te, stack);
		if(!worldIn.isRemote && te instanceof CrateTile) {
			ItemStack crate = new ItemStack(this);
			CrateBlockItem.setCrateData(crate, ((CrateTile)te).getCrateData());

			ItemEntity ent = new ItemEntity(worldIn, pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, crate);
			worldIn.addEntity(ent);
		}
	}

	
}
