package net.pirates.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.SwordItem;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.pirates.mod.items.prop.Prop;

public class MainSailBlock extends HorizontalFacingBaseBlock {

	public static final EnumProperty<SailType> TYPE = EnumProperty.create("type", SailType.class);
	
	public MainSailBlock() {
		super(Prop.Blocks.WOOL);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getCorrectStateInWorld(this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing()), context.getWorld(), context.getPos());
	}

	@Override
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
		return this.getCorrectStateInWorld(stateIn, worldIn, currentPos);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(TYPE);
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		// TODO Auto-generated method stub
		return super.getShape(state, worldIn, pos, context);
	}
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn,BlockRayTraceResult hit) {
		if(player.getHeldItem(handIn).getItem() instanceof SwordItem) {
			player.setMotion(player.getMotion().scale(0.1));
			player.fallDistance = player.fallDistance * 0.3F;
			if(!worldIn.isRemote && worldIn.getBlockState(pos).get(TYPE) == SailType.MIDDLE) {
				worldIn.setBlockState(pos, state.with(TYPE, SailType.RIPPED));
			}
			else worldIn.addParticle(new BlockParticleData(ParticleTypes.BLOCK, state), pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, 0 , 0, 0);
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

	public BlockState getCorrectStateInWorld(BlockState state, IWorld world, BlockPos pos) {
		
		//Water
		if(world.getFluidState(pos).isTagged(FluidTags.WATER) && world.getFluidState(pos).isSource()) {
			state = state.with(BlockStateProperties.WATERLOGGED, true);
		}
		
		boolean connected = world.getBlockState(pos.offset(state.get(BlockStateProperties.HORIZONTAL_FACING))).isSolid();
		
		//Types
		if(world.getBlockState(pos.down()).getBlock() != PBlocks.MAINSAIL) {
			//If no sail under, stop
			return state.with(TYPE, connected ? SailType.BOTTOM_CONNECT : SailType.BOTTOM);
		}
		if(world.getBlockState(pos.up()).getBlock() != PBlocks.MAINSAIL) {
			//If there's none above
			return state.with(TYPE, connected ? SailType.TOP_CONNECT : SailType.TOP);
		}
		//If would be middle, but is ripped
		if(state.get(TYPE) == SailType.RIPPED)
			return state.with(TYPE, SailType.RIPPED);
		return state.with(TYPE, connected ? SailType.MIDDLE_CONNECT : SailType.MIDDLE);
	}

	public static enum SailType implements IStringSerializable{
		TOP("top"),
		MIDDLE("middle"),
		BOTTOM("bottom"),
		TOP_CONNECT("top_connected"),
		MIDDLE_CONNECT("middle_connected"),
		BOTTOM_CONNECT("bottom_connected"),
		RIPPED("ripped");
		
		String name;
		
		SailType(String name){
			this.name = name;
		}

		@Override
		public String getName() {
			return name;
		}
	}
}
