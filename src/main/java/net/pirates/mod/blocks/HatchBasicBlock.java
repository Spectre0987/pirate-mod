package net.pirates.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.pirates.mod.items.prop.Prop;

public class HatchBasicBlock extends Block implements IWaterLoggable{

	public static final BooleanProperty OPEN = BooleanProperty.create("open");
	
	public VoxelShape openShape = createOpenShape();
	public VoxelShape closedShape = createClosedShape();
	
	public HatchBasicBlock() {
		super(Prop.Blocks.WOOD);
		this.setDefaultState(this.getDefaultState()
				.with(OPEN, false)
				.with(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH)
				.with(BlockStateProperties.WATERLOGGED, false));
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState()
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(OPEN, BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote)
			worldIn.setBlockState(pos, state.with(OPEN, !state.get(OPEN)));
		return true;
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public boolean isSolid(BlockState state) {
		return false;
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return state.get(OPEN) ? openShape : closedShape;
	}

	public VoxelShape createClosedShape() {
		VoxelShape shape = VoxelShapes.create(1.0, 0.0, -0.0625, 1.0625, 0.1875, 1.0625);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.0, 1.0, 1.0, 0.1875, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.0625, 0.0, -0.0625, 0.0, 0.1875, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.0, -0.0625, 1.0, 0.1875, 0.0), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.1875, -0.0625, 1.0, 0.25, 0.0), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.1875, 1.0, 1.0, 0.25, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.0625, 0.1875, -0.0625, 0.0, 0.25, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.46875, 0.1875, 0.53125, 0.53125, 0.25, 1.0), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.46875, 0.1875, 0.0, 0.53125, 0.25, 0.46875), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.1875, 0.46875, 1.0, 0.25, 0.53125), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.21875, -0.03125, 1.0, 0.21875, 1.03125), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(1.0, 0.1875, -0.0625, 1.0625, 0.25, 1.0625), IBooleanFunction.OR);
		return shape;
	}
	
	public VoxelShape createOpenShape() {
		
		VoxelShape shape = VoxelShapes.create(1.0, 0.0, -0.0625, 1.0625, 0.1875, 1.0625);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.0, 1.0, 1.0, 0.1875, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.0625, 0.0, -0.0625, 0.0, 0.1875, 1.0625), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.0, -0.0625, 1.0, 0.1875, 0.0), IBooleanFunction.OR);
		
		return shape;
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}


}
