package net.pirates.mod.blocks;

import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.pirates.mod.blocks.misc.DirectionalVoxelShape;
import net.pirates.mod.items.prop.Prop;

public class RailingBlock extends Block{

	public static BooleanProperty END = BooleanProperty.create("end");
	public DirectionalVoxelShape shape = new DirectionalVoxelShape(this.createVoxelShape());
	
	public RailingBlock() {
		super(Prop.Blocks.WOOD);
		this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false).with(END, true));
	}
	
	@Override
	public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	@Override
	public boolean canSpawnInBlock() {
		return false;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return this.shape.getShapeFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		if(context.getPlayer() == null)
			return this.getDefaultState();
		BlockState state = this.getDefaultState();
		
		state = state.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
		
		return state;
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(
				BlockStateProperties.HORIZONTAL_FACING,
				BlockStateProperties.WATERLOGGED,
				END);
	}

	@Override
	public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
		super.neighborChanged(state, worldIn, pos, blockIn, fromPos, isMoving);
		
		BlockState rail = worldIn.getBlockState(pos.offset(state.get(BlockStateProperties.HORIZONTAL_FACING)));
		
		//If this should connect, set this to not be an end
		if(rail.isSolid() || rail.getBlock() instanceof RailingBlock) {
			if(state.get(END)) {
				worldIn.setBlockState(pos, state.with(END, false));
			}
		}
		else if(!state.get(END)){
			worldIn.setBlockState(pos, state.with(END, true));
		}
	}
	
	public VoxelShape createVoxelShape() {
		return Stream.of(
				Block.makeCuboidShape(7, 2, 11.5, 9, 6, 13.5),
				Block.makeCuboidShape(7.35, 13, 0, 8.85, 14, 16),
				Block.makeCuboidShape(7.5, 8, 4, 8.5, 13, 5),
				Block.makeCuboidShape(7.5, 8, 8, 8.5, 13, 9),
				Block.makeCuboidShape(7.5, 8, 12, 8.5, 13, 13),
				Block.makeCuboidShape(7.25, 0, 3.75, 8.75, 8, 5.25),
				Block.makeCuboidShape(7.25, 0, 7.75, 8.75, 8, 9.25),
				Block.makeCuboidShape(7.25, 0, 11.75, 8.75, 8, 13.25),
				Block.makeCuboidShape(7, 2, 3.5, 9, 6, 5.5),
				Block.makeCuboidShape(7, 2, 7.5, 9, 6, 9.5)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
	}


}
